<?php

/* journal3/template/journal3/module/bottom_menu.twig */
class __TwigTemplate_494728ac5f40253ccc4020cb785a8aa2ef3510f915df06b9884eecc96cf01202 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 1
        if ((isset($context["items"]) ? $context["items"] : null)) {
            // line 2
            echo "  <div class=\"";
            echo $this->getAttribute((isset($context["j3"]) ? $context["j3"] : null), "classes", array(0 => (isset($context["classes"]) ? $context["classes"] : null)), "method");
            echo "\">
    <ul>
      ";
            // line 4
            $context['_parent'] = $context;
            $context['_seq'] = twig_ensure_traversable((isset($context["items"]) ? $context["items"] : null));
            foreach ($context['_seq'] as $context["_key"] => $context["item"]) {
                // line 5
                echo "        <li class=\"";
                echo $this->getAttribute((isset($context["j3"]) ? $context["j3"] : null), "classes", array(0 => $this->getAttribute($context["item"], "classes", array())), "method");
                echo "\">
          ";
                // line 6
                if ($this->getAttribute($this->getAttribute($context["item"], "link", array()), "href", array())) {
                    // line 7
                    echo "            <a href=\"";
                    echo $this->getAttribute($this->getAttribute($context["item"], "link", array()), "href", array());
                    echo "\" ";
                    echo $this->getAttribute((isset($context["j3"]) ? $context["j3"] : null), "linkAttrs", array(0 => $this->getAttribute($context["item"], "link", array())), "method");
                    echo ">";
                    echo $this->getAttribute((isset($context["j3"]) ? $context["j3"] : null), "countBadge", array(0 => $this->getAttribute($context["item"], "title", array()), 1 => $this->getAttribute($this->getAttribute($context["item"], "link", array()), "total", array()), 2 => $this->getAttribute($this->getAttribute($context["item"], "link", array()), "classes", array())), "method");
                    echo "</a>
          ";
                } else {
                    // line 9
                    echo "            <a>";
                    echo $this->getAttribute((isset($context["j3"]) ? $context["j3"] : null), "countBadge", array(0 => $this->getAttribute($context["item"], "title", array()), 1 => $this->getAttribute($this->getAttribute($context["item"], "link", array()), "total", array()), 2 => $this->getAttribute($this->getAttribute($context["item"], "link", array()), "classes", array())), "method");
                    echo "</a>
          ";
                }
                // line 11
                echo "        </li>
      ";
            }
            $_parent = $context['_parent'];
            unset($context['_seq'], $context['_iterated'], $context['_key'], $context['item'], $context['_parent'], $context['loop']);
            $context = array_intersect_key($context, $_parent) + $_parent;
            // line 13
            echo "    </ul>
  </div>
";
        }
    }

    public function getTemplateName()
    {
        return "journal3/template/journal3/module/bottom_menu.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  61 => 13,  54 => 11,  48 => 9,  38 => 7,  36 => 6,  31 => 5,  27 => 4,  21 => 2,  19 => 1,);
    }
}
/* {% if items %}*/
/*   <div class="{{ j3.classes(classes) }}">*/
/*     <ul>*/
/*       {% for item in items %}*/
/*         <li class="{{ j3.classes(item.classes) }}">*/
/*           {% if item.link.href %}*/
/*             <a href="{{ item.link.href }}" {{ j3.linkAttrs(item.link) }}>{{ j3.countBadge(item.title, item.link.total, item.link.classes) }}</a>*/
/*           {% else %}*/
/*             <a>{{ j3.countBadge(item.title, item.link.total, item.link.classes) }}</a>*/
/*           {% endif %}*/
/*         </li>*/
/*       {% endfor %}*/
/*     </ul>*/
/*   </div>*/
/* {% endif %}*/
/* */
