<?php

/* journal3/template/journal3/module/flyout_menu.twig */
class __TwigTemplate_aebef0d579d1ed8ba81c2b488596367d449e6e6d3439c764c1de07ef13d52e65 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 1
        $this->loadTemplate("journal3/template/journal3/module/main_menu.twig", "journal3/template/journal3/module/flyout_menu.twig", 1)->display($context);
    }

    public function getTemplateName()
    {
        return "journal3/template/journal3/module/flyout_menu.twig";
    }

    public function getDebugInfo()
    {
        return array (  19 => 1,);
    }
}
/* {% include 'journal3/template/journal3/module/main_menu.twig' %}*/
/* */
