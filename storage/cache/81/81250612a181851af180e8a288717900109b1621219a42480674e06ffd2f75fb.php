<?php

/* extension/payment/squareup.twig */
class __TwigTemplate_18c418e193376f28e22cf189d368e26f85b08612e2bfd2699f8c004314be888c extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 1
        echo (isset($context["header"]) ? $context["header"] : null);
        echo "
";
        // line 2
        echo (isset($context["column_left"]) ? $context["column_left"] : null);
        echo "
<div id=\"content\">
    <div class=\"page-header\">
        <div class=\"container-fluid\">
            <div class=\"pull-right\">
                <button type=\"submit\" form=\"form-square-checkout\" data-toggle=\"tooltip\" title=\"";
        // line 7
        echo (isset($context["button_save"]) ? $context["button_save"] : null);
        echo "\" class=\"btn btn-primary\"><i class=\"fa fa-save\"></i></button>
                <a href=\"";
        // line 8
        echo (isset($context["cancel"]) ? $context["cancel"] : null);
        echo "\" data-toggle=\"tooltip\" title=\"";
        echo (isset($context["button_cancel"]) ? $context["button_cancel"] : null);
        echo "\" class=\"btn btn-default\"><i class=\"fa fa-reply\"></i></a></div>
            <h1>";
        // line 9
        echo (isset($context["heading_title"]) ? $context["heading_title"] : null);
        echo "</h1>
            <ul class=\"breadcrumb\">
                ";
        // line 11
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable((isset($context["breadcrumbs"]) ? $context["breadcrumbs"] : null));
        foreach ($context['_seq'] as $context["_key"] => $context["breadcrumb"]) {
            // line 12
            echo "                    <li><a href=\"";
            echo $this->getAttribute($context["breadcrumb"], "href", array());
            echo "\">";
            echo $this->getAttribute($context["breadcrumb"], "text", array());
            echo "</a></li>
                ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['breadcrumb'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 14
        echo "            </ul>
        </div>
    </div>
    <div class=\"container-fluid\">
        ";
        // line 18
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable((isset($context["alerts"]) ? $context["alerts"] : null));
        foreach ($context['_seq'] as $context["_key"] => $context["alert"]) {
            // line 19
            echo "            <div class=\"alert alert-";
            echo $this->getAttribute($context["alert"], "type", array());
            echo "\"><i class=\"fa fa-";
            echo $this->getAttribute($context["alert"], "icon", array());
            echo "\"></i>&nbsp;";
            echo $this->getAttribute($context["alert"], "text", array());
            echo "
                <button type=\"button\" class=\"close\" data-dismiss=\"alert\">&times;</button>
            </div>
        ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['alert'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 23
        echo "
        <div class=\"panel panel-default\">
            <div class=\"panel-heading\">
                <h3 class=\"panel-title\"><i class=\"fa fa-pencil\"></i>&nbsp;";
        // line 26
        echo (isset($context["text_edit_heading"]) ? $context["text_edit_heading"] : null);
        echo "</h3>
            </div>
            <div class=\"panel-body\">
                <form action=\"";
        // line 29
        echo (isset($context["action"]) ? $context["action"] : null);
        echo "\" method=\"post\" enctype=\"multipart/form-data\" id=\"form-square-checkout\" class=\"form-horizontal\">
                    <input type=\"hidden\" name=\"payment_squareup_card\" value=\"1\" />
                    <ul class=\"nav nav-tabs\">
                        <li><a href=\"#tab-setting\" data-toggle=\"tab\"><i class=\"fa fa-gear\"></i>&nbsp;";
        // line 32
        echo (isset($context["tab_setting"]) ? $context["tab_setting"] : null);
        echo "</a></li>
                        <li><a href=\"#tab-transaction\" data-toggle=\"tab\"><i class=\"fa fa-list\"></i>&nbsp;";
        // line 33
        echo (isset($context["tab_transaction"]) ? $context["tab_transaction"] : null);
        echo "</a></li>
                        <li><a href=\"#tab-cron\" data-toggle=\"tab\"><i class=\"fa fa-clock-o\"></i>&nbsp;";
        // line 34
        echo (isset($context["tab_cron"]) ? $context["tab_cron"] : null);
        echo "</a></li>
                        <li><a href=\"#tab-recurring\" data-toggle=\"tab\"><i class=\"fa fa-hourglass-half\"></i>&nbsp;";
        // line 35
        echo (isset($context["tab_recurring"]) ? $context["tab_recurring"] : null);
        echo "</a></li>
                    </ul>
                    <div class=\"tab-content\">
                        <div class=\"tab-pane\" id=\"tab-setting\">
                            <fieldset>
                                ";
        // line 40
        if ((isset($context["payment_squareup_merchant_id"]) ? $context["payment_squareup_merchant_id"] : null)) {
            // line 41
            echo "                                    <legend>";
            echo (isset($context["text_connection_section"]) ? $context["text_connection_section"] : null);
            echo " - ";
            echo (isset($context["text_connected"]) ? $context["text_connected"] : null);
            echo "</legend>
                                    <div class=\"row\">
                                        <div class=\"col-sm-12\">
                                            <span data-toggle=\"tooltip\" title=\"";
            // line 44
            echo (isset($context["text_disabled_connect_help_text"]) ? $context["text_disabled_connect_help_text"] : null);
            echo "\">
                                                <a id=\"reconnect-button\" href=\"";
            // line 45
            echo (isset($context["payment_squareup_auth_link"]) ? $context["payment_squareup_auth_link"] : null);
            echo "\" class=\"btn btn-primary btn-lg btn-connect\" >";
            echo (isset($context["button_reconnect"]) ? $context["button_reconnect"] : null);
            echo "</a>
                                            </span>
                                            
                                            <span data-toggle=\"tooltip\" title=\"";
            // line 48
            echo (isset($context["text_disabled_connect_help_text"]) ? $context["text_disabled_connect_help_text"] : null);
            echo "\">
                                                <a id=\"refresh-button\" href=\"";
            // line 49
            echo (isset($context["payment_squareup_refresh_link"]) ? $context["payment_squareup_refresh_link"] : null);
            echo "\" class=\"btn btn-primary btn-lg btn-connect\" >";
            echo (isset($context["button_refresh"]) ? $context["button_refresh"] : null);
            echo "</a>
                                            </span>
                                            
                                            <p>";
            // line 52
            echo (isset($context["text_connected_info"]) ? $context["text_connected_info"] : null);
            echo "</p>
                                        </div>
                                    </div>
                                ";
        } else {
            // line 56
            echo "                                    <legend>";
            echo (isset($context["text_connection_section"]) ? $context["text_connection_section"] : null);
            echo " - ";
            echo (isset($context["text_not_connected"]) ? $context["text_not_connected"] : null);
            echo "</legend>
                                    <div class=\"row\">
                                        <div class=\"col-sm-12\">
                                            <span data-toggle=\"tooltip\" title=\"";
            // line 59
            echo (isset($context["text_disabled_connect_help_text"]) ? $context["text_disabled_connect_help_text"] : null);
            echo "\">
                                                <a id=\"connect-button\" href=\"";
            // line 60
            echo (isset($context["payment_squareup_auth_link"]) ? $context["payment_squareup_auth_link"] : null);
            echo "\" class=\"btn btn-primary btn-lg btn-connect\">";
            echo (isset($context["button_connect"]) ? $context["button_connect"] : null);
            echo "</a>
                                            </span>
                                            <p>";
            // line 62
            echo (isset($context["text_not_connected_info"]) ? $context["text_not_connected_info"] : null);
            echo "</p>
                                        </div>
                                    </div>
                                ";
        }
        // line 66
        echo "                            </fieldset>
                            <fieldset>
                                <legend>";
        // line 68
        echo (isset($context["text_settings_section_heading"]) ? $context["text_settings_section_heading"] : null);
        echo "</legend>
                                <div class=\"form-group\">
                                    <label class=\"col-sm-2 control-label\" for=\"dropdown_payment_squareup_status\"><span data-toggle=\"tooltip\" title=\"";
        // line 70
        echo (isset($context["text_extension_status_help"]) ? $context["text_extension_status_help"] : null);
        echo "\">";
        echo (isset($context["text_extension_status"]) ? $context["text_extension_status"] : null);
        echo "</span></label>
                                    <div class=\"col-sm-10\">
                                        <select name=\"payment_squareup_status\" id=\"dropdown_payment_squareup_status\" class=\"form-control\">
                                            <option value=\"1\" ";
        // line 73
        if (((isset($context["payment_squareup_status"]) ? $context["payment_squareup_status"] : null) == 1)) {
            echo " selected ";
        }
        echo ">";
        echo (isset($context["text_extension_status_enabled"]) ? $context["text_extension_status_enabled"] : null);
        echo "</option>
                                            <option value=\"0\" ";
        // line 74
        if (((isset($context["payment_squareup_status"]) ? $context["payment_squareup_status"] : null) == 0)) {
            echo " selected ";
        }
        echo ">";
        echo (isset($context["text_extension_status_disabled"]) ? $context["text_extension_status_disabled"] : null);
        echo "</option>
                                        </select>
                                    </div>
                                </div>
                                <div class=\"form-group\">
                                    <label class=\"col-sm-2 control-label\">
                                        <span data-toggle=\"tooltip\" title=\"";
        // line 80
        echo (isset($context["text_payment_method_name_help"]) ? $context["text_payment_method_name_help"] : null);
        echo "\">";
        echo (isset($context["text_payment_method_name_label"]) ? $context["text_payment_method_name_label"] : null);
        echo "</span>
                                    </label>
                                    <div class=\"col-sm-10\">
                                        ";
        // line 83
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable((isset($context["languages"]) ? $context["languages"] : null));
        foreach ($context['_seq'] as $context["_key"] => $context["language"]) {
            // line 84
            echo "                                            <div class=\"input-group\">
                                                <span class=\"input-group-addon\"><img src=\"";
            // line 85
            echo $this->getAttribute($context["language"], "image", array());
            echo "\" alt=\"";
            echo $this->getAttribute($context["language"], "name", array());
            echo "\" /></span>
                                                <input type=\"text\" name=\"payment_squareup_display_name[";
            // line 86
            echo $this->getAttribute($context["language"], "language_id", array());
            echo "]\" value=\"";
            echo ((($this->getAttribute((isset($context["payment_squareup_display_name"]) ? $context["payment_squareup_display_name"] : null), $this->getAttribute($context["language"], "language_id", array()), array(), "array", true, true) &&  !(null === $this->getAttribute((isset($context["payment_squareup_display_name"]) ? $context["payment_squareup_display_name"] : null), $this->getAttribute($context["language"], "language_id", array()), array(), "array")))) ? ($this->getAttribute((isset($context["payment_squareup_display_name"]) ? $context["payment_squareup_display_name"] : null), $this->getAttribute($context["language"], "language_id", array()), array(), "array")) : ((isset($context["text_payment_method_name_placeholder"]) ? $context["text_payment_method_name_placeholder"] : null)));
            echo "\" placeholder=\"";
            echo (isset($context["text_payment_method_name_placeholder"]) ? $context["text_payment_method_name_placeholder"] : null);
            echo "\" class=\"form-control\"/>
                                            </div>
                                        ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['language'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 89
        echo "                                    </div>
                                </div>
                                <div class=\"form-group\">
                                    <label class=\"col-sm-2 control-label\" for=\"input_payment_squareup_redirect_uri_static\">
                                        <span data-toggle=\"tooltip\" title=\"";
        // line 93
        echo (isset($context["text_redirect_uri_help"]) ? $context["text_redirect_uri_help"] : null);
        echo "\">";
        echo (isset($context["text_redirect_uri_label"]) ? $context["text_redirect_uri_label"] : null);
        echo "</span>
                                    </label>
                                    <div class=\"col-sm-10\">
                                        <input type=\"text\" id=\"input_payment_squareup_redirect_uri_static\" name=\"payment_squareup_redirect_uri_static\" value=\"";
        // line 96
        echo (isset($context["payment_squareup_redirect_uri"]) ? $context["payment_squareup_redirect_uri"] : null);
        echo "\" class=\"form-control\" disabled />
                                    </div>
                                </div>
                                <div class=\"form-group required\">
                                    <label class=\"col-sm-2 control-label\" for=\"input_payment_squareup_client_id\">
                                        <span data-toggle=\"tooltip\" title=\"";
        // line 101
        echo (isset($context["text_client_id_help"]) ? $context["text_client_id_help"] : null);
        echo "\">";
        echo (isset($context["text_client_id_label"]) ? $context["text_client_id_label"] : null);
        echo "</span>
                                    </label>
                                    <div class=\"col-sm-10\">
                                        <input type=\"text\" name=\"payment_squareup_client_id\" value=\"";
        // line 104
        echo (isset($context["payment_squareup_client_id"]) ? $context["payment_squareup_client_id"] : null);
        echo "\" placeholder=\"";
        echo (isset($context["text_client_id_placeholder"]) ? $context["text_client_id_placeholder"] : null);
        echo "\" id=\"input_payment_squareup_client_id\" class=\"form-control\"/>
                                        ";
        // line 105
        if ((isset($context["error_client_id"]) ? $context["error_client_id"] : null)) {
            // line 106
            echo "                                            <div class=\"text-danger\">";
            echo (isset($context["error_client_id"]) ? $context["error_client_id"] : null);
            echo "</div>
                                        ";
        }
        // line 108
        echo "                                    </div>
                                </div>
                                <div class=\"form-group required\">
                                    <label class=\"col-sm-2 control-label\" for=\"input_payment_squareup_client_secret\">
                                        <span data-toggle=\"tooltip\" title=\"";
        // line 112
        echo (isset($context["text_client_secret_help"]) ? $context["text_client_secret_help"] : null);
        echo "\">";
        echo (isset($context["text_client_secret_label"]) ? $context["text_client_secret_label"] : null);
        echo "</span>
                                    </label>
                                    <div class=\"col-sm-10\">
                                        <input type=\"text\" name=\"payment_squareup_client_secret\" value=\"";
        // line 115
        echo (isset($context["payment_squareup_client_secret"]) ? $context["payment_squareup_client_secret"] : null);
        echo "\" placeholder=\"";
        echo (isset($context["text_client_secret_placeholder"]) ? $context["text_client_secret_placeholder"] : null);
        echo "\" id=\"input_payment_squareup_client_secret\" class=\"form-control\"/>
                                        ";
        // line 116
        if ((isset($context["error_client_secret"]) ? $context["error_client_secret"] : null)) {
            // line 117
            echo "                                            <div class=\"text-danger\">";
            echo (isset($context["error_client_secret"]) ? $context["error_client_secret"] : null);
            echo "</div>
                                        ";
        }
        // line 119
        echo "                                    </div>
                                </div>
                                <div class=\"form-group\">
                                    <label class=\"col-sm-2 control-label\" for=\"dropdown_payment_squareup_delay_capture\"><span data-toggle=\"tooltip\" title=\"";
        // line 122
        echo (isset($context["text_delay_capture_help"]) ? $context["text_delay_capture_help"] : null);
        echo "\">";
        echo (isset($context["text_delay_capture_label"]) ? $context["text_delay_capture_label"] : null);
        echo "</span></label>
                                    <div class=\"col-sm-10\">
                                        <select name=\"payment_squareup_delay_capture\" id=\"dropdown_payment_squareup_delay_capture\" class=\"form-control\">
                                            <option value=\"1\" ";
        // line 125
        if (((isset($context["payment_squareup_delay_capture"]) ? $context["payment_squareup_delay_capture"] : null) == 1)) {
            echo " selected ";
        }
        echo ">";
        echo (isset($context["text_authorize_label"]) ? $context["text_authorize_label"] : null);
        echo "</option>
                                            <option value=\"0\" ";
        // line 126
        if (((isset($context["payment_squareup_delay_capture"]) ? $context["payment_squareup_delay_capture"] : null) == 0)) {
            echo " selected ";
        }
        echo ">";
        echo (isset($context["text_sale_label"]) ? $context["text_sale_label"] : null);
        echo "</option>
                                        </select>
                                    </div>
                                </div>
                                <div class=\"form-group\">
                                    <label class=\"col-sm-2 control-label\" for=\"input_payment_squareup_total\">
                                        <span data-toggle=\"tooltip\" title=\"";
        // line 132
        echo (isset($context["help_total"]) ? $context["help_total"] : null);
        echo "\">";
        echo (isset($context["entry_total"]) ? $context["entry_total"] : null);
        echo "</span>
                                    </label>
                                    <div class=\"col-sm-10\">
                                        <input type=\"text\" name=\"payment_squareup_total\" value=\"";
        // line 135
        echo (isset($context["payment_squareup_total"]) ? $context["payment_squareup_total"] : null);
        echo "\" placeholder=\"";
        echo (isset($context["entry_total"]) ? $context["entry_total"] : null);
        echo "\" id=\"payment_squareup_total\" class=\"form-control\"/>
                                    </div>
                                </div>
                                <div class=\"form-group\">
                                  <label class=\"col-sm-2 control-label\" for=\"input-geo-zone\">";
        // line 139
        echo (isset($context["entry_geo_zone"]) ? $context["entry_geo_zone"] : null);
        echo "</label>
                                  <div class=\"col-sm-10\">
                                    <select name=\"payment_squareup_geo_zone_id\" id=\"input-geo-zone\" class=\"form-control\">
                                        <option value=\"0\">";
        // line 142
        echo (isset($context["text_all_zones"]) ? $context["text_all_zones"] : null);
        echo "</option>
                                        ";
        // line 143
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable((isset($context["geo_zones"]) ? $context["geo_zones"] : null));
        foreach ($context['_seq'] as $context["_key"] => $context["geo_zone"]) {
            // line 144
            echo "                                            <option value=\"";
            echo $this->getAttribute($context["geo_zone"], "geo_zone_id", array());
            echo "\" ";
            if (($this->getAttribute($context["geo_zone"], "geo_zone_id", array()) == (isset($context["payment_squareup_geo_zone_id"]) ? $context["payment_squareup_geo_zone_id"] : null))) {
                echo " selected ";
            }
            echo ">";
            echo $this->getAttribute($context["geo_zone"], "name", array());
            echo "</option>
                                        ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['geo_zone'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 146
        echo "                                    </select>
                                  </div>
                                </div>
                                <div class=\"form-group\">
                                    <label class=\"col-sm-2 control-label\" for=\"input_payment_squareup_sort_order\">
                                        ";
        // line 151
        echo (isset($context["entry_sort_order"]) ? $context["entry_sort_order"] : null);
        echo "
                                    </label>
                                    <div class=\"col-sm-10\">
                                        <input type=\"text\" name=\"payment_squareup_sort_order\" value=\"";
        // line 154
        echo (isset($context["payment_squareup_sort_order"]) ? $context["payment_squareup_sort_order"] : null);
        echo "\" placeholder=\"";
        echo (isset($context["entry_sort_order"]) ? $context["entry_sort_order"] : null);
        echo "\" id=\"input_payment_squareup_sort_order\" class=\"form-control\"/>
                                    </div>
                                </div>
                                <div class=\"form-group\">
                                    <label class=\"col-sm-2 control-label\" for=\"dropdown_payment_squareup_debug\"><span data-toggle=\"tooltip\" title=\"";
        // line 158
        echo (isset($context["text_debug_help"]) ? $context["text_debug_help"] : null);
        echo "\">";
        echo (isset($context["text_debug_label"]) ? $context["text_debug_label"] : null);
        echo "</span></label>
                                    <div class=\"col-sm-10\">
                                        <select name=\"payment_squareup_debug\" id=\"dropdown_payment_squareup_debug\" class=\"form-control\">
                                            <option value=\"1\" ";
        // line 161
        if (((isset($context["payment_squareup_debug"]) ? $context["payment_squareup_debug"] : null) == 1)) {
            echo " selected ";
        }
        echo ">";
        echo (isset($context["text_debug_enabled"]) ? $context["text_debug_enabled"] : null);
        echo "</option>
                                            <option value=\"0\" ";
        // line 162
        if (((isset($context["payment_squareup_debug"]) ? $context["payment_squareup_debug"] : null) == 0)) {
            echo " selected ";
        }
        echo ">";
        echo (isset($context["text_debug_disabled"]) ? $context["text_debug_disabled"] : null);
        echo "</option>
                                        </select>
                                    </div>
                                </div>
                                <div class=\"form-group\">
                                    <label class=\"col-sm-2 control-label\" for=\"dropdown_payment_squareup_enable_sandbox\"><span data-toggle=\"tooltip\" title=\"";
        // line 167
        echo (isset($context["text_enable_sandbox_help"]) ? $context["text_enable_sandbox_help"] : null);
        echo "\">";
        echo (isset($context["text_enable_sandbox_label"]) ? $context["text_enable_sandbox_label"] : null);
        echo "</span></label>
                                    <div class=\"col-sm-10\">
                                        <select name=\"payment_squareup_enable_sandbox\" id=\"dropdown_payment_squareup_enable_sandbox\" class=\"form-control\">
                                            <option value=\"1\" ";
        // line 170
        if (((isset($context["payment_squareup_enable_sandbox"]) ? $context["payment_squareup_enable_sandbox"] : null) == 1)) {
            echo " selected ";
        }
        echo ">";
        echo (isset($context["text_sandbox_enabled_label"]) ? $context["text_sandbox_enabled_label"] : null);
        echo "</option>
                                            <option value=\"0\" ";
        // line 171
        if (((isset($context["payment_squareup_enable_sandbox"]) ? $context["payment_squareup_enable_sandbox"] : null) == 0)) {
            echo " selected ";
        }
        echo ">";
        echo (isset($context["text_sandbox_disabled_label"]) ? $context["text_sandbox_disabled_label"] : null);
        echo "</option>
                                        </select>
                                    </div>
                                </div>
                            </fieldset>
                            <fieldset id=\"sandbox_settings\" ";
        // line 176
        if ( !(isset($context["payment_squareup_enable_sandbox"]) ? $context["payment_squareup_enable_sandbox"] : null)) {
            echo " style=\"display: none;\" ";
        }
        echo "> 
                                <legend>";
        // line 177
        echo (isset($context["text_sandbox_section_heading"]) ? $context["text_sandbox_section_heading"] : null);
        echo "</legend>
                                <div class=\"form-group required\">
                                    <label class=\"col-sm-2 control-label\" for=\"input_payment_squareup_sandbox_client_id\">
                                        <span data-toggle=\"tooltip\" title=\"";
        // line 180
        echo (isset($context["text_sandbox_client_id_help"]) ? $context["text_sandbox_client_id_help"] : null);
        echo "\">";
        echo (isset($context["text_sandbox_client_id_label"]) ? $context["text_sandbox_client_id_label"] : null);
        echo "</span>
                                    </label>
                                    <div class=\"col-sm-10\">
                                        <input type=\"text\" name=\"payment_squareup_sandbox_client_id\" value=\"";
        // line 183
        echo (isset($context["payment_squareup_sandbox_client_id"]) ? $context["payment_squareup_sandbox_client_id"] : null);
        echo "\" placeholder=\"";
        echo (isset($context["text_sandbox_client_id_placeholder"]) ? $context["text_sandbox_client_id_placeholder"] : null);
        echo "\" id=\"input_payment_squareup_sandbox_client_id\" class=\"form-control\" />
                                        ";
        // line 184
        if ((isset($context["error_sandbox_client_id"]) ? $context["error_sandbox_client_id"] : null)) {
            // line 185
            echo "                                            <div class=\"text-danger\">";
            echo (isset($context["error_sandbox_client_id"]) ? $context["error_sandbox_client_id"] : null);
            echo "</div>
                                        ";
        }
        // line 187
        echo "                                    </div>
                                </div>
                                <div class=\"form-group required\">
                                    <label class=\"col-sm-2 control-label\" for=\"input_payment_squareup_sandbox_token\">
                                        <span data-toggle=\"tooltip\" title=\"";
        // line 191
        echo (isset($context["text_sandbox_access_token_help"]) ? $context["text_sandbox_access_token_help"] : null);
        echo "\">";
        echo (isset($context["text_sandbox_access_token_label"]) ? $context["text_sandbox_access_token_label"] : null);
        echo "</span>
                                    </label>
                                    <div class=\"col-sm-10\">
                                        <input type=\"text\" name=\"payment_squareup_sandbox_token\" value=\"";
        // line 194
        echo (isset($context["payment_squareup_sandbox_token"]) ? $context["payment_squareup_sandbox_token"] : null);
        echo "\" placeholder=\"";
        echo (isset($context["text_sandbox_access_token_placeholder"]) ? $context["text_sandbox_access_token_placeholder"] : null);
        echo "\" id=\"input_payment_squareup_sandbox_token\" class=\"form-control\"/>
                                        ";
        // line 195
        if ((isset($context["error_sandbox_token"]) ? $context["error_sandbox_token"] : null)) {
            // line 196
            echo "                                            <div class=\"text-danger\">";
            echo (isset($context["error_sandbox_token"]) ? $context["error_sandbox_token"] : null);
            echo "</div>
                                        ";
        }
        // line 198
        echo "                                    </div>
                                </div>
                            </fieldset>
                            <fieldset>
                                <legend>";
        // line 202
        echo (isset($context["text_merchant_info_section_heading"]) ? $context["text_merchant_info_section_heading"] : null);
        echo "</legend>  
                                <div class=\"form-group\">
                                    <label class=\"col-sm-2 control-label\" for=\"input_merchant_name\">
                                        ";
        // line 205
        echo (isset($context["text_merchant_name_label"]) ? $context["text_merchant_name_label"] : null);
        echo "
                                    </label>
                                    <div class=\"col-sm-10\">
                                        <input type=\"text\" name=\"merchant_name\" value=\"";
        // line 208
        echo (isset($context["payment_squareup_merchant_name"]) ? $context["payment_squareup_merchant_name"] : null);
        echo "\" placeholder=\"";
        echo (isset($context["text_merchant_name_placeholder"]) ? $context["text_merchant_name_placeholder"] : null);
        echo "\" id=\"input_merchant_name\" class=\"form-control\" disabled/>
                                    </div>
                                </div>
                                <div class=\"form-group\">
                                    <label class=\"col-sm-2 control-label\" for=\"access_token_expires_time\">
                                        ";
        // line 213
        echo (isset($context["text_access_token_expires_label"]) ? $context["text_access_token_expires_label"] : null);
        echo "
                                    </label>
                                    <div class=\"col-sm-10\">
                                        <input type=\"text\" name=\"access_token_expires\" value=\"";
        // line 216
        echo (isset($context["access_token_expires_time"]) ? $context["access_token_expires_time"] : null);
        echo "\" placeholder=\"";
        echo (isset($context["text_access_token_expires_placeholder"]) ? $context["text_access_token_expires_placeholder"] : null);
        echo "\" id=\"access_token_expires_time\" class=\"form-control\" disabled />
                                    </div>
                                </div>
                                <div class=\"form-group required\">
                                    <label class=\"col-sm-2 control-label\" for=\"dropdown_payment_squareup_sandbox_location_id\"><span data-toggle=\"tooltip\" title=\"";
        // line 220
        echo (isset($context["text_location_help"]) ? $context["text_location_help"] : null);
        echo "\">";
        echo (isset($context["text_location_label"]) ? $context["text_location_label"] : null);
        echo "</span></label>
                                    <div class=\"col-sm-10\">
                                        <select name=\"payment_squareup_location_id\" id=\"dropdown_payment_squareup_location_id\" class=\"form-control\" ";
        // line 222
        if ( !(isset($context["payment_squareup_locations"]) ? $context["payment_squareup_locations"] : null)) {
            echo " disabled ";
        }
        echo ">
                                            ";
        // line 223
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable((isset($context["payment_squareup_locations"]) ? $context["payment_squareup_locations"] : null));
        foreach ($context['_seq'] as $context["_key"] => $context["location"]) {
            // line 224
            echo "                                                <option value=\"";
            echo $this->getAttribute($context["location"], "id", array());
            echo "\" ";
            if (($this->getAttribute($context["location"], "id", array()) == (isset($context["payment_squareup_location_id"]) ? $context["payment_squareup_location_id"] : null))) {
                echo " selected ";
            }
            echo ">";
            echo $this->getAttribute($context["location"], "name", array());
            echo "</option>
                                            ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['location'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 226
        echo "                                        </select>
                                        <select name=\"payment_squareup_sandbox_location_id\" id=\"dropdown_payment_squareup_sandbox_location_id\" class=\"form-control\" ";
        // line 227
        if ( !(isset($context["payment_squareup_sandbox_locations"]) ? $context["payment_squareup_sandbox_locations"] : null)) {
            echo " disabled ";
        }
        echo ">
                                            ";
        // line 228
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable((isset($context["payment_squareup_sandbox_locations"]) ? $context["payment_squareup_sandbox_locations"] : null));
        foreach ($context['_seq'] as $context["_key"] => $context["location"]) {
            // line 229
            echo "                                                <option value=\"";
            echo $this->getAttribute($context["location"], "id", array());
            echo "\" ";
            if (($this->getAttribute($context["location"], "id", array()) == (isset($context["payment_squareup_sandbox_location_id"]) ? $context["payment_squareup_sandbox_location_id"] : null))) {
                echo " selected ";
            }
            echo ">";
            echo $this->getAttribute($context["location"], "name", array());
            echo "</option>
                                            ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['location'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 231
        echo "                                        </select>
                                        ";
        // line 232
        if ((isset($context["error_location"]) ? $context["error_location"] : null)) {
            // line 233
            echo "                                            <div class=\"text-danger\">";
            echo (isset($context["error_location"]) ? $context["error_location"] : null);
            echo "</div>
                                        ";
        }
        // line 235
        echo "                                    </div>
                                </div>
                            </fieldset>
                            <fieldset>
                                <legend>";
        // line 239
        echo (isset($context["text_transaction_statuses"]) ? $context["text_transaction_statuses"] : null);
        echo "</legend>
                                <div class=\"form-group\">
                                    <label class=\"col-sm-2 control-label\" for=\"dropdown_payment_squareup_status_authorized\"><span data-toggle=\"tooltip\" title=\"";
        // line 241
        echo (isset($context["payment_squareup_status_comment_authorized"]) ? $context["payment_squareup_status_comment_authorized"] : null);
        echo "\">";
        echo (isset($context["entry_status_authorized"]) ? $context["entry_status_authorized"] : null);
        echo "</span></label>
                                    <div class=\"col-sm-10\">
                                        <select name=\"payment_squareup_status_authorized\" id=\"dropdown_payment_squareup_status_authorized\" class=\"form-control\">
                                            ";
        // line 244
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable((isset($context["order_statuses"]) ? $context["order_statuses"] : null));
        foreach ($context['_seq'] as $context["_key"] => $context["order_status"]) {
            // line 245
            echo "                                                <option value=\"";
            echo $this->getAttribute($context["order_status"], "order_status_id", array());
            echo "\" ";
            if (($this->getAttribute($context["order_status"], "order_status_id", array()) == (isset($context["payment_squareup_status_authorized"]) ? $context["payment_squareup_status_authorized"] : null))) {
                echo " selected ";
            }
            echo ">";
            echo $this->getAttribute($context["order_status"], "name", array());
            echo "</option>
                                            ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['order_status'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 247
        echo "                                        </select>
                                    </div>
                                </div>
                                <div class=\"form-group\">
                                    <label class=\"col-sm-2 control-label\" for=\"dropdown_payment_squareup_status_captured\"><span data-toggle=\"tooltip\" title=\"";
        // line 251
        echo (isset($context["payment_squareup_status_comment_captured"]) ? $context["payment_squareup_status_comment_captured"] : null);
        echo "\">";
        echo (isset($context["entry_status_captured"]) ? $context["entry_status_captured"] : null);
        echo "</span></label>
                                    <div class=\"col-sm-10\">
                                        <select name=\"payment_squareup_status_captured\" id=\"dropdown_payment_squareup_status_captured\" class=\"form-control\">
                                            ";
        // line 254
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable((isset($context["order_statuses"]) ? $context["order_statuses"] : null));
        foreach ($context['_seq'] as $context["_key"] => $context["order_status"]) {
            // line 255
            echo "                                                <option value=\"";
            echo $this->getAttribute($context["order_status"], "order_status_id", array());
            echo "\" ";
            if (($this->getAttribute($context["order_status"], "order_status_id", array()) == (isset($context["payment_squareup_status_captured"]) ? $context["payment_squareup_status_captured"] : null))) {
                echo " selected ";
            }
            echo ">";
            echo $this->getAttribute($context["order_status"], "name", array());
            echo "</option>
                                            ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['order_status'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 257
        echo "                                        </select>
                                    </div>
                                </div>
                                <div class=\"form-group\">
                                    <label class=\"col-sm-2 control-label\" for=\"dropdown_payment_squareup_status_voided\"><span data-toggle=\"tooltip\" title=\"";
        // line 261
        echo (isset($context["payment_squareup_status_comment_voided"]) ? $context["payment_squareup_status_comment_voided"] : null);
        echo "\">";
        echo (isset($context["entry_status_voided"]) ? $context["entry_status_voided"] : null);
        echo "</span></label>
                                    <div class=\"col-sm-10\">
                                        <select name=\"payment_squareup_status_voided\" id=\"dropdown_payment_squareup_status_voided\" class=\"form-control\">
                                            ";
        // line 264
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable((isset($context["order_statuses"]) ? $context["order_statuses"] : null));
        foreach ($context['_seq'] as $context["_key"] => $context["order_status"]) {
            // line 265
            echo "                                                <option value=\"";
            echo $this->getAttribute($context["order_status"], "order_status_id", array());
            echo "\" ";
            if (($this->getAttribute($context["order_status"], "order_status_id", array()) == (isset($context["payment_squareup_status_voided"]) ? $context["payment_squareup_status_voided"] : null))) {
                echo " selected ";
            }
            echo ">";
            echo $this->getAttribute($context["order_status"], "name", array());
            echo "</option>
                                            ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['order_status'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 267
        echo "                                        </select>
                                    </div>
                                </div>
                                <div class=\"form-group\">
                                    <label class=\"col-sm-2 control-label\" for=\"dropdown_payment_squareup_status_failed\"><span data-toggle=\"tooltip\" title=\"";
        // line 271
        echo (isset($context["payment_squareup_status_comment_failed"]) ? $context["payment_squareup_status_comment_failed"] : null);
        echo "\">";
        echo (isset($context["entry_status_failed"]) ? $context["entry_status_failed"] : null);
        echo "</span></label>
                                    <div class=\"col-sm-10\">
                                        <select name=\"payment_squareup_status_failed\" id=\"dropdown_payment_squareup_status_failed\" class=\"form-control\">
                                            ";
        // line 274
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable((isset($context["order_statuses"]) ? $context["order_statuses"] : null));
        foreach ($context['_seq'] as $context["_key"] => $context["order_status"]) {
            // line 275
            echo "                                                <option value=\"";
            echo $this->getAttribute($context["order_status"], "order_status_id", array());
            echo "\" ";
            if (($this->getAttribute($context["order_status"], "order_status_id", array()) == (isset($context["payment_squareup_status_failed"]) ? $context["payment_squareup_status_failed"] : null))) {
                echo " selected ";
            }
            echo ">";
            echo $this->getAttribute($context["order_status"], "name", array());
            echo "</option>
                                            ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['order_status'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 277
        echo "                                        </select>
                                    </div>
                                </div>
                            </fieldset>
                        </div>
                        <div class=\"tab-pane\" id=\"tab-transaction\">
                            <div id=\"transaction-alert\" data-message=\"";
        // line 283
        echo (isset($context["text_loading"]) ? $context["text_loading"] : null);
        echo "\"></div>
                            <div class=\"text-right margin-bottom\">
                            </div>
                            <div class=\"table-responsive\">
                                <table class=\"table table-bordered table-hover\">
                                    <thead>
                                        <tr>
                                            <th class=\"text-left hidden-xs\">";
        // line 290
        echo (isset($context["column_transaction_id"]) ? $context["column_transaction_id"] : null);
        echo "</th> 
                                            <th class=\"text-left\">";
        // line 291
        echo (isset($context["column_customer"]) ? $context["column_customer"] : null);
        echo "</th>
                                            <th class=\"text-left hidden-xs\">";
        // line 292
        echo (isset($context["column_order_id"]) ? $context["column_order_id"] : null);
        echo "</th>
                                            <th class=\"text-left hidden-xs\">";
        // line 293
        echo (isset($context["column_type"]) ? $context["column_type"] : null);
        echo "</th>
                                            <th class=\"text-left hidden-xs\">";
        // line 294
        echo (isset($context["column_amount"]) ? $context["column_amount"] : null);
        echo "</th>
                                            <th class=\"text-left hidden-xs\">";
        // line 295
        echo (isset($context["column_refunds"]) ? $context["column_refunds"] : null);
        echo "</th>
                                            <th class=\"text-left hidden-xs hidden-sm\">";
        // line 296
        echo (isset($context["column_ip"]) ? $context["column_ip"] : null);
        echo "</th>
                                            <th class=\"text-left\">";
        // line 297
        echo (isset($context["column_date_created"]) ? $context["column_date_created"] : null);
        echo "</th>
                                            <th class=\"text-right\">";
        // line 298
        echo (isset($context["column_action"]) ? $context["column_action"] : null);
        echo "</th>
                                        </tr>
                                    </thead>
                                    <tbody id=\"transactions\">
                                    </tbody>
                                </table>
                                <div id=\"transactions_pagination\"></div>
                            </div>
                        </div>
                        <div class=\"tab-pane\" id=\"tab-cron\">
                            <fieldset>
                                <legend>";
        // line 309
        echo (isset($context["text_executables"]) ? $context["text_executables"] : null);
        echo "</legend>
                                <div class=\"alert alert-info\"><i class=\"fa fa-info-circle\"></i> ";
        // line 310
        echo (isset($context["text_recurring_info"]) ? $context["text_recurring_info"] : null);
        echo "</div>
                                <div class=\"form-group\">
                                    <label class=\"col-sm-2 control-label\"><span data-toggle=\"tooltip\" data-original-title=\"";
        // line 312
        echo (isset($context["help_local_cron"]) ? $context["help_local_cron"] : null);
        echo "\">";
        echo (isset($context["text_local_cron"]) ? $context["text_local_cron"] : null);
        echo "</span></label>
                                    <div class=\"col-sm-10\">
                                        <input disabled type=\"text\" class=\"form-control\" value=\"";
        // line 314
        echo (isset($context["payment_squareup_cron_command"]) ? $context["payment_squareup_cron_command"] : null);
        echo "\" />
                                    </div>
                                </div>
                                <div class=\"form-group\">
                                    <label class=\"col-sm-2 control-label\"><span data-toggle=\"tooltip\" data-original-title=\"";
        // line 318
        echo (isset($context["help_remote_cron"]) ? $context["help_remote_cron"] : null);
        echo "\">";
        echo (isset($context["text_remote_cron"]) ? $context["text_remote_cron"] : null);
        echo "</span></label>
                                    <div class=\"col-sm-10\">
                                        <div class=\"input-group\">
                                            <input disabled type=\"text\" name=\"payment_squareup_cron_url\" id=\"input_payment_squareup_cron_url\" class=\"form-control\" value=\"\" />
                                            <div data-toggle=\"tooltip\" data-original-title=\"";
        // line 322
        echo (isset($context["text_refresh_token"]) ? $context["text_refresh_token"] : null);
        echo "\" class=\"input-group-addon btn btn-primary\" id=\"refresh-cron-token\">
                                                <i class=\"fa fa-refresh\"></i>
                                            </div>
                                        </div>
                                        <input id=\"input_payment_squareup_cron_token\" type=\"hidden\" name=\"payment_squareup_cron_token\" value=\"";
        // line 326
        echo (isset($context["payment_squareup_cron_token"]) ? $context["payment_squareup_cron_token"] : null);
        echo "\" />
                                    </div>
                                </div>
                                <div class=\"form-group required\">
                                    <label class=\"col-sm-2 control-label\" for=\"checkbox_payment_squareup_cron_acknowledge\">";
        // line 330
        echo (isset($context["entry_setup_confirmation"]) ? $context["entry_setup_confirmation"] : null);
        echo "</label>
                                    <div class=\"col-sm-10\">
                                        <label class=\"checkbox-inline\">
                                            <input id=\"checkbox_payment_squareup_cron_acknowledge\" type=\"checkbox\" value=\"1\" ";
        // line 333
        if ((isset($context["payment_squareup_cron_acknowledge"]) ? $context["payment_squareup_cron_acknowledge"] : null)) {
            echo " checked ";
        }
        echo " name=\"payment_squareup_cron_acknowledge\" /> ";
        echo (isset($context["text_acknowledge_cron"]) ? $context["text_acknowledge_cron"] : null);
        echo "
                                        </label>

                                        ";
        // line 336
        if ((isset($context["error_cron_acknowledge"]) ? $context["error_cron_acknowledge"] : null)) {
            // line 337
            echo "                                            <div class=\"text-danger\">";
            echo (isset($context["error_cron_acknowledge"]) ? $context["error_cron_acknowledge"] : null);
            echo "</div>
                                        ";
        }
        // line 339
        echo "                                    </div>
                                </div>
                            </fieldset>
                            <fieldset>
                                <legend>";
        // line 343
        echo (isset($context["text_admin_notifications"]) ? $context["text_admin_notifications"] : null);
        echo "</legend>
                                <div class=\"form-group\">
                                    <label class=\"control-label col-sm-2\" for=\"dropdown_payment_squareup_cron_email_status\"><span data-toggle=\"tooltip\" data-original-title=\"";
        // line 345
        echo (isset($context["help_cron_email_status"]) ? $context["help_cron_email_status"] : null);
        echo "\">";
        echo (isset($context["text_cron_email_status"]) ? $context["text_cron_email_status"] : null);
        echo "</span></label>
                                    <div class=\"col-sm-10\">
                                        <select id=\"dropdown_payment_squareup_cron_email_status\" name=\"payment_squareup_cron_email_status\" class=\"form-control\">
                                            <option value=\"1\" ";
        // line 348
        if (((isset($context["payment_squareup_cron_email_status"]) ? $context["payment_squareup_cron_email_status"] : null) == "1")) {
            echo " selected ";
        }
        echo ">";
        echo (isset($context["text_enabled"]) ? $context["text_enabled"] : null);
        echo "</option>
                                            <option value=\"0\" ";
        // line 349
        if (((isset($context["payment_squareup_cron_email_status"]) ? $context["payment_squareup_cron_email_status"] : null) == "0")) {
            echo " selected ";
        }
        echo ">";
        echo (isset($context["text_disabled"]) ? $context["text_disabled"] : null);
        echo "</option>
                                        </select>
                                    </div>
                                </div>
                                <div class=\"form-group required\">
                                    <label class=\"col-sm-2 control-label\" for=\"input_payment_squareup_cron_email\"><span data-toggle=\"tooltip\" data-original-title=\"";
        // line 354
        echo (isset($context["help_cron_email"]) ? $context["help_cron_email"] : null);
        echo "\">";
        echo (isset($context["text_cron_email"]) ? $context["text_cron_email"] : null);
        echo "</span></label>
                                    <div class=\"col-sm-10\">
                                        <input id=\"input_payment_squareup_cron_email\" name=\"payment_squareup_cron_email\" type=\"text\" class=\"form-control\" value=\"";
        // line 356
        echo (isset($context["payment_squareup_cron_email"]) ? $context["payment_squareup_cron_email"] : null);
        echo "\"/>
                                        ";
        // line 357
        if ((isset($context["error_cron_email"]) ? $context["error_cron_email"] : null)) {
            // line 358
            echo "                                            <div class=\"text-danger\">";
            echo (isset($context["error_cron_email"]) ? $context["error_cron_email"] : null);
            echo "</div>
                                        ";
        }
        // line 360
        echo "                                    </div>
                                </div>
                            </fieldset>
                        </div>
                        <div class=\"tab-pane\" id=\"tab-recurring\">
                            <div class=\"form-group\">
                                <label class=\"control-label col-sm-2\" for=\"dropdown_payment_squareup_recurring_status\"><span data-toggle=\"tooltip\" data-original-title=\"";
        // line 366
        echo (isset($context["help_recurring_status"]) ? $context["help_recurring_status"] : null);
        echo "\">";
        echo (isset($context["text_recurring_status"]) ? $context["text_recurring_status"] : null);
        echo "</span></label>
                                <div class=\"col-sm-10\">
                                    <select id=\"dropdown_payment_squareup_recurring_status\" name=\"payment_squareup_recurring_status\" class=\"form-control\">
                                        <option value=\"1\" ";
        // line 369
        if (((isset($context["payment_squareup_recurring_status"]) ? $context["payment_squareup_recurring_status"] : null) == "1")) {
            echo " selected ";
        }
        echo ">";
        echo (isset($context["text_enabled"]) ? $context["text_enabled"] : null);
        echo "</option>
                                        <option value=\"0\" ";
        // line 370
        if (((isset($context["payment_squareup_recurring_status"]) ? $context["payment_squareup_recurring_status"] : null) == "0")) {
            echo " selected ";
        }
        echo ">";
        echo (isset($context["text_disabled"]) ? $context["text_disabled"] : null);
        echo "</option>
                                    </select>
                                </div>
                            </div>
                            <fieldset>
                                <legend>";
        // line 375
        echo (isset($context["text_customer_notifications"]) ? $context["text_customer_notifications"] : null);
        echo "</legend>
                                <div class=\"form-group\">
                                    <label class=\"control-label col-sm-2\" for=\"dropdown_payment_squareup_notify_recurring_success\"><span data-toggle=\"tooltip\" data-original-title=\"";
        // line 377
        echo (isset($context["help_notify_recurring_success"]) ? $context["help_notify_recurring_success"] : null);
        echo "\">";
        echo (isset($context["text_notify_recurring_success"]) ? $context["text_notify_recurring_success"] : null);
        echo "</span></label>
                                    <div class=\"col-sm-10\">
                                        <select id=\"dropdown_payment_squareup_notify_recurring_success\" name=\"payment_squareup_notify_recurring_success\" class=\"form-control\">
                                            <option value=\"1\" ";
        // line 380
        if (((isset($context["payment_squareup_notify_recurring_success"]) ? $context["payment_squareup_notify_recurring_success"] : null) == "1")) {
            echo " selected ";
        }
        echo ">";
        echo (isset($context["text_enabled"]) ? $context["text_enabled"] : null);
        echo "</option>
                                            <option value=\"0\" ";
        // line 381
        if (((isset($context["payment_squareup_notify_recurring_success"]) ? $context["payment_squareup_notify_recurring_success"] : null) == "0")) {
            echo " selected ";
        }
        echo ">";
        echo (isset($context["text_disabled"]) ? $context["text_disabled"] : null);
        echo "</option>
                                        </select>
                                    </div>
                                </div>
                                <div class=\"form-group\">
                                    <label class=\"control-label col-sm-2\" for=\"dropdown_payment_squareup_notify_recurring_fail\"><span data-toggle=\"tooltip\" data-original-title=\"";
        // line 386
        echo (isset($context["help_notify_recurring_fail"]) ? $context["help_notify_recurring_fail"] : null);
        echo "\">";
        echo (isset($context["text_notify_recurring_fail"]) ? $context["text_notify_recurring_fail"] : null);
        echo "</span></label>
                                    <div class=\"col-sm-10\">
                                        <select id=\"dropdown_payment_squareup_notify_recurring_fail\" name=\"payment_squareup_notify_recurring_fail\" class=\"form-control\">
                                            <option value=\"1\" ";
        // line 389
        if (((isset($context["payment_squareup_notify_recurring_fail"]) ? $context["payment_squareup_notify_recurring_fail"] : null) == "1")) {
            echo " selected ";
        }
        echo ">";
        echo (isset($context["text_enabled"]) ? $context["text_enabled"] : null);
        echo "</option>
                                            <option value=\"0\" ";
        // line 390
        if (((isset($context["payment_squareup_notify_recurring_fail"]) ? $context["payment_squareup_notify_recurring_fail"] : null) == "0")) {
            echo " selected ";
        }
        echo ">";
        echo (isset($context["text_disabled"]) ? $context["text_disabled"] : null);
        echo "</option>
                                        </select>
                                    </div>
                                </div>
                            </fieldset>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
    <div class=\"modal fade\" id=\"squareup-confirm-modal\">
        <div class=\"modal-dialog\">
            <div class=\"modal-content\">
                <div class=\"modal-header\">
                    <button type=\"button\" class=\"close\" data-dismiss=\"modal\" aria-hidden=\"true\">&times;</button>
                    <h4 class=\"modal-title\">";
        // line 406
        echo (isset($context["text_confirm_action"]) ? $context["text_confirm_action"] : null);
        echo "</h4>
                </div>
                <div class=\"modal-body\">
                    <h4 id=\"squareup-confirm-modal-content\"></h4>
                </div>
                <div class=\"modal-footer\">
                    <button type=\"button\" class=\"btn btn-default\" data-dismiss=\"modal\">";
        // line 412
        echo (isset($context["text_close"]) ? $context["text_close"] : null);
        echo "</button>
                    <button id=\"squareup-confirm-ok\" type=\"button\" class=\"btn btn-primary\">";
        // line 413
        echo (isset($context["text_ok"]) ? $context["text_ok"] : null);
        echo "</button>
                </div>
            </div>
        </div>
    </div>
    <div class=\"modal fade\" id=\"squareup-refund-modal\">
        <div class=\"modal-dialog\">
            <div class=\"modal-content\">
                <div class=\"modal-header\">
                    <button type=\"button\" class=\"close\" data-dismiss=\"modal\" aria-hidden=\"true\">&times;</button>
                    <h4 class=\"modal-title\">";
        // line 423
        echo (isset($context["text_refund_details"]) ? $context["text_refund_details"] : null);
        echo "</h4>
                </div>
                <div class=\"modal-body\">
                    <div class=\"form-group\">
                        <label class=\"control-label\" id=\"squareup-refund-modal-content-reason\"></label>
                        <textarea class=\"form-control\" id=\"squareup-refund-reason\" required></textarea>
                    </div>
                    <div class=\"form-group\">
                        <label class=\"control-label\" id=\"squareup-refund-modal-content-amount\"></label>
                        <input class=\"form-control\" type=\"text\" id=\"squareup-refund-amount\" required />
                    </div>
                </div>
                <div class=\"modal-footer\">
                    <button type=\"button\" class=\"btn btn-default\" data-dismiss=\"modal\">";
        // line 436
        echo (isset($context["text_close"]) ? $context["text_close"] : null);
        echo "</button>
                    <button id=\"squareup-refund-ok\" type=\"button\" class=\"btn btn-primary\">";
        // line 437
        echo (isset($context["text_ok"]) ? $context["text_ok"] : null);
        echo "</button>
                </div>
            </div>
        </div>
    </div>
</div>
<script type=\"text/javascript\">
\$(document).ready(function() {
    var triggerConnectButtons = function() {
        if (\$('#input_payment_squareup_client_id').val() != '' && \$('#input_payment_squareup_client_secret').val() != '') {
            \$('.btn-connect').removeClass('disabled');
        } else {
            \$('.btn-connect').addClass('disabled');
        }
    }

    var setCronUrl = function() {
        \$('#input_payment_squareup_cron_url').val(
            \"";
        // line 455
        echo (isset($context["payment_squareup_cron_url"]) ? $context["payment_squareup_cron_url"] : null);
        echo "\".replace('{CRON_TOKEN}', \$('#input_payment_squareup_cron_token').val())
        );
    }

    var randomString = function() {
        return (Math.random() * 100).toString(16).replace('.', '');
    }

    var onConnectClick = function(event) {
        if (\$('#input_payment_squareup_client_id').val() != '";
        // line 464
        echo (isset($context["payment_squareup_client_id"]) ? $context["payment_squareup_client_id"] : null);
        echo "' || \$('#input_payment_squareup_client_secret').val() != '";
        echo (isset($context["payment_squareup_client_secret"]) ? $context["payment_squareup_client_secret"] : null);
        echo "') {
            event.preventDefault();
            event.stopPropagation();

            \$('form').attr('action','";
        // line 468
        echo (isset($context["action_save_auth"]) ? $context["action_save_auth"] : null);
        echo "').submit();
        }
    }

    var listTransactions = function(page) {
        \$.ajax({
          url : '";
        // line 474
        echo (isset($context["url_list_transactions"]) ? $context["url_list_transactions"] : null);
        echo "'.replace('{PAGE}', page ? page : transactionListPage),
          dataType : 'json',
          beforeSend : function() {
            \$('#refresh_transactions').button('loading');
            \$('#transactions_pagination').empty();
            \$('#transactions').html('<tr><td colspan=\"9\" class=\"text-center\"><i class=\"fa fa-circle-o-notch fa-spin\"></i>&nbsp;";
        // line 479
        echo (isset($context["text_loading"]) ? $context["text_loading"] : null);
        echo "</td></tr>');
          },
          success : function(data) {
            var html = '';

            if (data.transactions.length) {
              for (var i in data.transactions) {
                var row = data.transactions[i];

                html += '<tr>';
                html += '<td class=\"text-left hidden-xs\">' + row.transaction_id + '</td>';
                html += '<td class=\"text-left hidden-xs\">' + row.customer + '</td>';
                html += '<td class=\"text-left\"><a target=\"_blank\" href=\"' + row.url_order + '\">' + row.order_id + '</td>';
                html += '<td class=\"text-left hidden-xs\">' + row.type + '</td>';
                html += '<td class=\"text-left hidden-xs\">' + row.amount + '</td>';
                html += '<td class=\"text-left hidden-xs\">' + row.num_refunds + '</td>';
                html += '<td class=\"text-left hidden-xs hidden-sm\">' + row.ip + '</td>';
                html += '<td class=\"text-left\">' + row.date_created + '</td>';
                html += '<td class=\"text-right\">';

                switch (row.type) {
                    case \"AUTHORIZED\" : {
                        html += '<a class=\"btn btn-success\" data-url-transaction-capture=\"' + row.url_capture + '\" data-confirm-capture=\"' + row.confirm_capture + '\">";
        // line 501
        echo (isset($context["text_capture"]) ? $context["text_capture"] : null);
        echo "</a> ';
                        html += '<a class=\"btn btn-warning\" data-url-transaction-void=\"' + row.url_void + '\" data-confirm-void=\"' + row.confirm_void + '\">";
        // line 502
        echo (isset($context["text_void"]) ? $context["text_void"] : null);
        echo "</a> ';
                    } break;

                    case \"CAPTURED\" : {
                        html += '<a class=\"btn btn-danger\" data-url-transaction-refund=\"' + row.url_refund + '\" data-confirm-refund=\"' + row.confirm_refund + '\" data-insert-amount=\"' + row.insert_amount + '\">";
        // line 506
        echo (isset($context["text_refund"]) ? $context["text_refund"] : null);
        echo "</a> ';
                    } break;
                }

                html += ' <a class=\"btn btn-info\" href=\"' + row.url_info + '\">";
        // line 510
        echo (isset($context["text_view"]) ? $context["text_view"] : null);
        echo "</a>';
                html += '</td>';
                html += '</tr>';
              }
            } else {
              html += '<tr>';
              html += '<td class=\"text-center\" colspan=\"9\">";
        // line 516
        echo (isset($context["text_no_transactions"]) ? $context["text_no_transactions"] : null);
        echo "</td>';
              html += '</tr>';
            }

            \$('#transactions').html(html);
            
            \$('#transactions_pagination').html(data.pagination).find('a[href]').each(function(index,element) {
              \$(this).click(function(e) {
                e.preventDefault();

                transactionListPage = isNaN(\$(this).attr('href')) ? 1 : \$(this).attr('href');

                listTransactions();
              })
            });
          },
          complete : function() {
            \$('#refresh_transactions').button('reset');
          }
        });
    }

    var transactionLoading = function() {
        var message = \$('#transaction-alert').attr('data-message');

        \$('#transaction-alert').html('<div class=\"text-center alert alert-info\"><i class=\"fa fa-circle-o-notch fa-spin\"></i>&nbsp;' + message + '</div>');
    }

    var transactionError = function(message) {
        \$('#transaction-alert').html('<div class=\"alert alert-danger\"><button type=\"button\" class=\"close\" data-dismiss=\"alert\" aria-label=\"X\"><span aria-hidden=\"true\">&times;</span></button><i class=\"fa fa-exclamation-circle\"></i>&nbsp;' + message + '</div>');
    }

    var transactionSuccess = function(message) {
        \$('#transaction-alert').html('<div class=\"alert alert-success\"><button type=\"button\" class=\"close\" data-dismiss=\"alert\" aria-label=\"X\"><span aria-hidden=\"true\">&times;</span></button><i class=\"fa fa-exclamation-circle\"></i>&nbsp;' + message + '</div>');
    }

    var addOrderHistory = function(data, success_callback) {
        \$.ajax({
            url: '";
        // line 554
        echo (isset($context["catalog"]) ? $context["catalog"] : null);
        echo "index.php?route=api/order/history&api_token=";
        echo (isset($context["api_token"]) ? $context["api_token"] : null);
        echo "&order_id=' + data.order_id,
            type: 'post',
            dataType: 'json',
            data: data,
            success: function(json) {
                if (json['error']) {
                    transactionError(json['error']);
                    enableTransactionButtons();
                }

                if (json['success']) {
                    success_callback();
                }
            },
            error: function(xhr, ajaxOptions, thrownError) {
                transactionError(thrownError + \"\\r\\n\" + xhr.statusText + \"\\r\\n\" + xhr.responseText);
                enableTransactionButtons();
            }
        });
    }

    var transactionRequest = function(type, url, data) {
        \$.ajax({
            url : url,
            dataType : 'json',
            type : type,
            data : data,
            beforeSend : transactionLoading,
            success : function(json) {
                if (json.error) {
                    transactionError(json.error);
                    enableTransactionButtons();
                }

                if (json.success && json.order_history_data) {
                    addOrderHistory(json.order_history_data, function() {
                        transactionSuccess(json.success);
                        listTransactions();
                    });
                }
            },
            error : function(xhr, ajaxSettings, thrownError) {
                transactionError(thrownError + \"\\r\\n\" + xhr.statusText + \"\\r\\n\" + xhr.responseText);
                enableTransactionButtons();
            }
        });
    }

    var disableTransactionButtons = function() {
        \$('*[data-url-transaction-capture], *[data-url-transaction-void], *[data-url-transaction-refund]').attr('disabled', true);
    }

    var enableTransactionButtons = function() {
        \$('*[data-url-transaction-capture], *[data-url-transaction-void], *[data-url-transaction-refund]').attr('disabled', false);
    }

    var modalConfirm = function(url, text) {
        var modal = '#squareup-confirm-modal';
        var content = '#squareup-confirm-modal-content';
        var button = '#squareup-confirm-ok';

        \$(content).html(text);
        \$(button).unbind().click(function() {
            disableTransactionButtons();

            \$(modal).modal('hide');

            transactionRequest('GET', url);
        });
        
        \$(modal).modal('show');
    }

    var refundInputValidate = function(reason_input, amount_input) {
        var result = true;

        if (!\$(reason_input)[0].checkValidity()) {
            \$(reason_input).closest('.form-group').addClass('has-error');
            result = false;
        } else {
            \$(reason_input).closest('.form-group').removeClass('has-error');
        }

        if (!\$(amount_input)[0].checkValidity()) {
            \$(amount_input).closest('.form-group').addClass('has-error');
            result = false;
        } else {
            \$(amount_input).closest('.form-group').removeClass('has-error');
        }

        return result;
    }

    var modalRefund = function(url, text_reason, text_amount) {
        var modal = '#squareup-refund-modal';
        var content_reason = '#squareup-refund-modal-content-reason';
        var content_amount = '#squareup-refund-modal-content-amount';
        var button = '#squareup-refund-ok';
        var reason_input = '#squareup-refund-reason';
        var amount_input = '#squareup-refund-amount';

        \$(content_reason).html(text_reason);
        \$(content_amount).html(text_amount);

        \$(reason_input).val('');
        \$(amount_input).val('');

        \$(button).unbind().click(function() {
            if (!refundInputValidate(reason_input, amount_input)) {
                return;
            }

            disableTransactionButtons();

            \$(modal).modal('hide');

            transactionRequest('POST', url, {
                reason : \$(reason_input).val(),
                amount : \$(amount_input).val()
            });
        });
        
        \$(modal).modal('show');
    }

    var transactionListPage = 1;

    \$('.nav-tabs a[href=\"#";
        // line 681
        echo (isset($context["tab"]) ? $context["tab"] : null);
        echo "\"]').tab('show');

    \$('#dropdown_payment_squareup_enable_sandbox')
        .change(function() {
            if (\$(this).val() == '0') {
                \$('#sandbox_settings').slideUp();
                \$('#dropdown_payment_squareup_location_id').show();
                \$('#dropdown_payment_squareup_sandbox_location_id').hide();
            } else {
                \$('#sandbox_settings').slideDown();
                \$('#dropdown_payment_squareup_location_id').hide();
                \$('#dropdown_payment_squareup_sandbox_location_id').show();
            }
        })
        .trigger('change');

    \$('#input_payment_squareup_client_id, #input_payment_squareup_client_secret')
        .change(triggerConnectButtons)
        .keyup(triggerConnectButtons)
        .trigger('change');

    \$('#refresh-cron-token').click(function() {
        \$('#input_payment_squareup_cron_token').val(randomString() + randomString());
        setCronUrl();
    });

    \$('#connect-button').click(onConnectClick);
    
    \$('#reconnect-button').click(onConnectClick);

    \$(document).on('click', '*[data-url-transaction-capture]', function() {
        if (\$(this).attr('disabled')) return;

        modalConfirm(
            \$(this).attr('data-url-transaction-capture'),
            \$(this).attr('data-confirm-capture')
        );
    });
        
    \$(document).on('click', '*[data-url-transaction-void]', function() {
        if (\$(this).attr('disabled')) return;

        modalConfirm(
            \$(this).attr('data-url-transaction-void'),
            \$(this).attr('data-confirm-void')
        );
    });

    \$(document).on('click', '*[data-url-transaction-refund]', function() {
        if (\$(this).attr('disabled')) return;

        modalRefund(
            \$(this).attr('data-url-transaction-refund'),
            \$(this).attr('data-confirm-refund'),
            \$(this).attr('data-insert-amount')
        );
    });
    
    setCronUrl();

    listTransactions();
});
</script>
";
        // line 744
        echo (isset($context["footer"]) ? $context["footer"] : null);
    }

    public function getTemplateName()
    {
        return "extension/payment/squareup.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  1503 => 744,  1437 => 681,  1305 => 554,  1264 => 516,  1255 => 510,  1248 => 506,  1241 => 502,  1237 => 501,  1212 => 479,  1204 => 474,  1195 => 468,  1186 => 464,  1174 => 455,  1153 => 437,  1149 => 436,  1133 => 423,  1120 => 413,  1116 => 412,  1107 => 406,  1084 => 390,  1076 => 389,  1068 => 386,  1056 => 381,  1048 => 380,  1040 => 377,  1035 => 375,  1023 => 370,  1015 => 369,  1007 => 366,  999 => 360,  993 => 358,  991 => 357,  987 => 356,  980 => 354,  968 => 349,  960 => 348,  952 => 345,  947 => 343,  941 => 339,  935 => 337,  933 => 336,  923 => 333,  917 => 330,  910 => 326,  903 => 322,  894 => 318,  887 => 314,  880 => 312,  875 => 310,  871 => 309,  857 => 298,  853 => 297,  849 => 296,  845 => 295,  841 => 294,  837 => 293,  833 => 292,  829 => 291,  825 => 290,  815 => 283,  807 => 277,  792 => 275,  788 => 274,  780 => 271,  774 => 267,  759 => 265,  755 => 264,  747 => 261,  741 => 257,  726 => 255,  722 => 254,  714 => 251,  708 => 247,  693 => 245,  689 => 244,  681 => 241,  676 => 239,  670 => 235,  664 => 233,  662 => 232,  659 => 231,  644 => 229,  640 => 228,  634 => 227,  631 => 226,  616 => 224,  612 => 223,  606 => 222,  599 => 220,  590 => 216,  584 => 213,  574 => 208,  568 => 205,  562 => 202,  556 => 198,  550 => 196,  548 => 195,  542 => 194,  534 => 191,  528 => 187,  522 => 185,  520 => 184,  514 => 183,  506 => 180,  500 => 177,  494 => 176,  482 => 171,  474 => 170,  466 => 167,  454 => 162,  446 => 161,  438 => 158,  429 => 154,  423 => 151,  416 => 146,  401 => 144,  397 => 143,  393 => 142,  387 => 139,  378 => 135,  370 => 132,  357 => 126,  349 => 125,  341 => 122,  336 => 119,  330 => 117,  328 => 116,  322 => 115,  314 => 112,  308 => 108,  302 => 106,  300 => 105,  294 => 104,  286 => 101,  278 => 96,  270 => 93,  264 => 89,  251 => 86,  245 => 85,  242 => 84,  238 => 83,  230 => 80,  217 => 74,  209 => 73,  201 => 70,  196 => 68,  192 => 66,  185 => 62,  178 => 60,  174 => 59,  165 => 56,  158 => 52,  150 => 49,  146 => 48,  138 => 45,  134 => 44,  125 => 41,  123 => 40,  115 => 35,  111 => 34,  107 => 33,  103 => 32,  97 => 29,  91 => 26,  86 => 23,  71 => 19,  67 => 18,  61 => 14,  50 => 12,  46 => 11,  41 => 9,  35 => 8,  31 => 7,  23 => 2,  19 => 1,);
    }
}
/* {{ header }}*/
/* {{ column_left }}*/
/* <div id="content">*/
/*     <div class="page-header">*/
/*         <div class="container-fluid">*/
/*             <div class="pull-right">*/
/*                 <button type="submit" form="form-square-checkout" data-toggle="tooltip" title="{{ button_save }}" class="btn btn-primary"><i class="fa fa-save"></i></button>*/
/*                 <a href="{{ cancel }}" data-toggle="tooltip" title="{{ button_cancel }}" class="btn btn-default"><i class="fa fa-reply"></i></a></div>*/
/*             <h1>{{ heading_title }}</h1>*/
/*             <ul class="breadcrumb">*/
/*                 {% for breadcrumb in breadcrumbs %}*/
/*                     <li><a href="{{ breadcrumb.href }}">{{ breadcrumb.text }}</a></li>*/
/*                 {% endfor %}*/
/*             </ul>*/
/*         </div>*/
/*     </div>*/
/*     <div class="container-fluid">*/
/*         {% for alert in alerts %}*/
/*             <div class="alert alert-{{ alert.type }}"><i class="fa fa-{{ alert.icon }}"></i>&nbsp;{{ alert.text }}*/
/*                 <button type="button" class="close" data-dismiss="alert">&times;</button>*/
/*             </div>*/
/*         {% endfor %}*/
/* */
/*         <div class="panel panel-default">*/
/*             <div class="panel-heading">*/
/*                 <h3 class="panel-title"><i class="fa fa-pencil"></i>&nbsp;{{ text_edit_heading }}</h3>*/
/*             </div>*/
/*             <div class="panel-body">*/
/*                 <form action="{{ action }}" method="post" enctype="multipart/form-data" id="form-square-checkout" class="form-horizontal">*/
/*                     <input type="hidden" name="payment_squareup_card" value="1" />*/
/*                     <ul class="nav nav-tabs">*/
/*                         <li><a href="#tab-setting" data-toggle="tab"><i class="fa fa-gear"></i>&nbsp;{{ tab_setting }}</a></li>*/
/*                         <li><a href="#tab-transaction" data-toggle="tab"><i class="fa fa-list"></i>&nbsp;{{ tab_transaction }}</a></li>*/
/*                         <li><a href="#tab-cron" data-toggle="tab"><i class="fa fa-clock-o"></i>&nbsp;{{ tab_cron }}</a></li>*/
/*                         <li><a href="#tab-recurring" data-toggle="tab"><i class="fa fa-hourglass-half"></i>&nbsp;{{ tab_recurring }}</a></li>*/
/*                     </ul>*/
/*                     <div class="tab-content">*/
/*                         <div class="tab-pane" id="tab-setting">*/
/*                             <fieldset>*/
/*                                 {% if payment_squareup_merchant_id %}*/
/*                                     <legend>{{ text_connection_section }} - {{ text_connected }}</legend>*/
/*                                     <div class="row">*/
/*                                         <div class="col-sm-12">*/
/*                                             <span data-toggle="tooltip" title="{{ text_disabled_connect_help_text }}">*/
/*                                                 <a id="reconnect-button" href="{{ payment_squareup_auth_link }}" class="btn btn-primary btn-lg btn-connect" >{{ button_reconnect}}</a>*/
/*                                             </span>*/
/*                                             */
/*                                             <span data-toggle="tooltip" title="{{ text_disabled_connect_help_text }}">*/
/*                                                 <a id="refresh-button" href="{{ payment_squareup_refresh_link }}" class="btn btn-primary btn-lg btn-connect" >{{ button_refresh }}</a>*/
/*                                             </span>*/
/*                                             */
/*                                             <p>{{ text_connected_info}}</p>*/
/*                                         </div>*/
/*                                     </div>*/
/*                                 {% else %}*/
/*                                     <legend>{{ text_connection_section }} - {{ text_not_connected }}</legend>*/
/*                                     <div class="row">*/
/*                                         <div class="col-sm-12">*/
/*                                             <span data-toggle="tooltip" title="{{ text_disabled_connect_help_text }}">*/
/*                                                 <a id="connect-button" href="{{ payment_squareup_auth_link }}" class="btn btn-primary btn-lg btn-connect">{{ button_connect }}</a>*/
/*                                             </span>*/
/*                                             <p>{{ text_not_connected_info }}</p>*/
/*                                         </div>*/
/*                                     </div>*/
/*                                 {% endif %}*/
/*                             </fieldset>*/
/*                             <fieldset>*/
/*                                 <legend>{{ text_settings_section_heading }}</legend>*/
/*                                 <div class="form-group">*/
/*                                     <label class="col-sm-2 control-label" for="dropdown_payment_squareup_status"><span data-toggle="tooltip" title="{{ text_extension_status_help }}">{{ text_extension_status }}</span></label>*/
/*                                     <div class="col-sm-10">*/
/*                                         <select name="payment_squareup_status" id="dropdown_payment_squareup_status" class="form-control">*/
/*                                             <option value="1" {% if payment_squareup_status == 1 %} selected {% endif %}>{{ text_extension_status_enabled }}</option>*/
/*                                             <option value="0" {% if payment_squareup_status == 0 %} selected {% endif %}>{{ text_extension_status_disabled }}</option>*/
/*                                         </select>*/
/*                                     </div>*/
/*                                 </div>*/
/*                                 <div class="form-group">*/
/*                                     <label class="col-sm-2 control-label">*/
/*                                         <span data-toggle="tooltip" title="{{ text_payment_method_name_help }}">{{ text_payment_method_name_label }}</span>*/
/*                                     </label>*/
/*                                     <div class="col-sm-10">*/
/*                                         {% for language in languages %}*/
/*                                             <div class="input-group">*/
/*                                                 <span class="input-group-addon"><img src="{{ language.image }}" alt="{{ language.name }}" /></span>*/
/*                                                 <input type="text" name="payment_squareup_display_name[{{ language.language_id }}]" value="{{ payment_squareup_display_name[language.language_id] ?? text_payment_method_name_placeholder }}" placeholder="{{ text_payment_method_name_placeholder }}" class="form-control"/>*/
/*                                             </div>*/
/*                                         {% endfor %}*/
/*                                     </div>*/
/*                                 </div>*/
/*                                 <div class="form-group">*/
/*                                     <label class="col-sm-2 control-label" for="input_payment_squareup_redirect_uri_static">*/
/*                                         <span data-toggle="tooltip" title="{{ text_redirect_uri_help }}">{{ text_redirect_uri_label }}</span>*/
/*                                     </label>*/
/*                                     <div class="col-sm-10">*/
/*                                         <input type="text" id="input_payment_squareup_redirect_uri_static" name="payment_squareup_redirect_uri_static" value="{{ payment_squareup_redirect_uri }}" class="form-control" disabled />*/
/*                                     </div>*/
/*                                 </div>*/
/*                                 <div class="form-group required">*/
/*                                     <label class="col-sm-2 control-label" for="input_payment_squareup_client_id">*/
/*                                         <span data-toggle="tooltip" title="{{ text_client_id_help }}">{{ text_client_id_label }}</span>*/
/*                                     </label>*/
/*                                     <div class="col-sm-10">*/
/*                                         <input type="text" name="payment_squareup_client_id" value="{{ payment_squareup_client_id }}" placeholder="{{ text_client_id_placeholder }}" id="input_payment_squareup_client_id" class="form-control"/>*/
/*                                         {% if error_client_id %}*/
/*                                             <div class="text-danger">{{ error_client_id }}</div>*/
/*                                         {% endif %}*/
/*                                     </div>*/
/*                                 </div>*/
/*                                 <div class="form-group required">*/
/*                                     <label class="col-sm-2 control-label" for="input_payment_squareup_client_secret">*/
/*                                         <span data-toggle="tooltip" title="{{ text_client_secret_help }}">{{ text_client_secret_label }}</span>*/
/*                                     </label>*/
/*                                     <div class="col-sm-10">*/
/*                                         <input type="text" name="payment_squareup_client_secret" value="{{ payment_squareup_client_secret }}" placeholder="{{ text_client_secret_placeholder }}" id="input_payment_squareup_client_secret" class="form-control"/>*/
/*                                         {% if error_client_secret %}*/
/*                                             <div class="text-danger">{{ error_client_secret }}</div>*/
/*                                         {% endif %}*/
/*                                     </div>*/
/*                                 </div>*/
/*                                 <div class="form-group">*/
/*                                     <label class="col-sm-2 control-label" for="dropdown_payment_squareup_delay_capture"><span data-toggle="tooltip" title="{{ text_delay_capture_help }}">{{ text_delay_capture_label }}</span></label>*/
/*                                     <div class="col-sm-10">*/
/*                                         <select name="payment_squareup_delay_capture" id="dropdown_payment_squareup_delay_capture" class="form-control">*/
/*                                             <option value="1" {% if payment_squareup_delay_capture == 1 %} selected {% endif %}>{{ text_authorize_label }}</option>*/
/*                                             <option value="0" {% if payment_squareup_delay_capture == 0 %} selected {% endif %}>{{ text_sale_label }}</option>*/
/*                                         </select>*/
/*                                     </div>*/
/*                                 </div>*/
/*                                 <div class="form-group">*/
/*                                     <label class="col-sm-2 control-label" for="input_payment_squareup_total">*/
/*                                         <span data-toggle="tooltip" title="{{ help_total }}">{{ entry_total }}</span>*/
/*                                     </label>*/
/*                                     <div class="col-sm-10">*/
/*                                         <input type="text" name="payment_squareup_total" value="{{ payment_squareup_total }}" placeholder="{{ entry_total }}" id="payment_squareup_total" class="form-control"/>*/
/*                                     </div>*/
/*                                 </div>*/
/*                                 <div class="form-group">*/
/*                                   <label class="col-sm-2 control-label" for="input-geo-zone">{{ entry_geo_zone }}</label>*/
/*                                   <div class="col-sm-10">*/
/*                                     <select name="payment_squareup_geo_zone_id" id="input-geo-zone" class="form-control">*/
/*                                         <option value="0">{{ text_all_zones }}</option>*/
/*                                         {% for geo_zone in geo_zones %}*/
/*                                             <option value="{{ geo_zone.geo_zone_id }}" {% if geo_zone.geo_zone_id == payment_squareup_geo_zone_id %} selected {% endif %}>{{ geo_zone.name }}</option>*/
/*                                         {% endfor %}*/
/*                                     </select>*/
/*                                   </div>*/
/*                                 </div>*/
/*                                 <div class="form-group">*/
/*                                     <label class="col-sm-2 control-label" for="input_payment_squareup_sort_order">*/
/*                                         {{ entry_sort_order }}*/
/*                                     </label>*/
/*                                     <div class="col-sm-10">*/
/*                                         <input type="text" name="payment_squareup_sort_order" value="{{ payment_squareup_sort_order }}" placeholder="{{ entry_sort_order }}" id="input_payment_squareup_sort_order" class="form-control"/>*/
/*                                     </div>*/
/*                                 </div>*/
/*                                 <div class="form-group">*/
/*                                     <label class="col-sm-2 control-label" for="dropdown_payment_squareup_debug"><span data-toggle="tooltip" title="{{ text_debug_help }}">{{ text_debug_label }}</span></label>*/
/*                                     <div class="col-sm-10">*/
/*                                         <select name="payment_squareup_debug" id="dropdown_payment_squareup_debug" class="form-control">*/
/*                                             <option value="1" {% if payment_squareup_debug == 1 %} selected {% endif %}>{{ text_debug_enabled }}</option>*/
/*                                             <option value="0" {% if payment_squareup_debug == 0 %} selected {% endif %}>{{ text_debug_disabled }}</option>*/
/*                                         </select>*/
/*                                     </div>*/
/*                                 </div>*/
/*                                 <div class="form-group">*/
/*                                     <label class="col-sm-2 control-label" for="dropdown_payment_squareup_enable_sandbox"><span data-toggle="tooltip" title="{{ text_enable_sandbox_help }}">{{ text_enable_sandbox_label }}</span></label>*/
/*                                     <div class="col-sm-10">*/
/*                                         <select name="payment_squareup_enable_sandbox" id="dropdown_payment_squareup_enable_sandbox" class="form-control">*/
/*                                             <option value="1" {% if payment_squareup_enable_sandbox == 1 %} selected {% endif %}>{{ text_sandbox_enabled_label }}</option>*/
/*                                             <option value="0" {% if payment_squareup_enable_sandbox == 0 %} selected {% endif %}>{{ text_sandbox_disabled_label }}</option>*/
/*                                         </select>*/
/*                                     </div>*/
/*                                 </div>*/
/*                             </fieldset>*/
/*                             <fieldset id="sandbox_settings" {% if not payment_squareup_enable_sandbox %} style="display: none;" {% endif %}> */
/*                                 <legend>{{ text_sandbox_section_heading }}</legend>*/
/*                                 <div class="form-group required">*/
/*                                     <label class="col-sm-2 control-label" for="input_payment_squareup_sandbox_client_id">*/
/*                                         <span data-toggle="tooltip" title="{{ text_sandbox_client_id_help }}">{{ text_sandbox_client_id_label }}</span>*/
/*                                     </label>*/
/*                                     <div class="col-sm-10">*/
/*                                         <input type="text" name="payment_squareup_sandbox_client_id" value="{{ payment_squareup_sandbox_client_id }}" placeholder="{{ text_sandbox_client_id_placeholder }}" id="input_payment_squareup_sandbox_client_id" class="form-control" />*/
/*                                         {% if error_sandbox_client_id %}*/
/*                                             <div class="text-danger">{{ error_sandbox_client_id }}</div>*/
/*                                         {% endif %}*/
/*                                     </div>*/
/*                                 </div>*/
/*                                 <div class="form-group required">*/
/*                                     <label class="col-sm-2 control-label" for="input_payment_squareup_sandbox_token">*/
/*                                         <span data-toggle="tooltip" title="{{ text_sandbox_access_token_help }}">{{ text_sandbox_access_token_label }}</span>*/
/*                                     </label>*/
/*                                     <div class="col-sm-10">*/
/*                                         <input type="text" name="payment_squareup_sandbox_token" value="{{ payment_squareup_sandbox_token }}" placeholder="{{ text_sandbox_access_token_placeholder }}" id="input_payment_squareup_sandbox_token" class="form-control"/>*/
/*                                         {% if error_sandbox_token %}*/
/*                                             <div class="text-danger">{{ error_sandbox_token }}</div>*/
/*                                         {% endif %}*/
/*                                     </div>*/
/*                                 </div>*/
/*                             </fieldset>*/
/*                             <fieldset>*/
/*                                 <legend>{{ text_merchant_info_section_heading }}</legend>  */
/*                                 <div class="form-group">*/
/*                                     <label class="col-sm-2 control-label" for="input_merchant_name">*/
/*                                         {{ text_merchant_name_label }}*/
/*                                     </label>*/
/*                                     <div class="col-sm-10">*/
/*                                         <input type="text" name="merchant_name" value="{{ payment_squareup_merchant_name }}" placeholder="{{ text_merchant_name_placeholder }}" id="input_merchant_name" class="form-control" disabled/>*/
/*                                     </div>*/
/*                                 </div>*/
/*                                 <div class="form-group">*/
/*                                     <label class="col-sm-2 control-label" for="access_token_expires_time">*/
/*                                         {{ text_access_token_expires_label }}*/
/*                                     </label>*/
/*                                     <div class="col-sm-10">*/
/*                                         <input type="text" name="access_token_expires" value="{{ access_token_expires_time }}" placeholder="{{ text_access_token_expires_placeholder }}" id="access_token_expires_time" class="form-control" disabled />*/
/*                                     </div>*/
/*                                 </div>*/
/*                                 <div class="form-group required">*/
/*                                     <label class="col-sm-2 control-label" for="dropdown_payment_squareup_sandbox_location_id"><span data-toggle="tooltip" title="{{ text_location_help }}">{{ text_location_label }}</span></label>*/
/*                                     <div class="col-sm-10">*/
/*                                         <select name="payment_squareup_location_id" id="dropdown_payment_squareup_location_id" class="form-control" {% if not payment_squareup_locations %} disabled {% endif %}>*/
/*                                             {% for location in payment_squareup_locations %}*/
/*                                                 <option value="{{ location.id }}" {% if location.id == payment_squareup_location_id %} selected {% endif %}>{{ location.name }}</option>*/
/*                                             {% endfor %}*/
/*                                         </select>*/
/*                                         <select name="payment_squareup_sandbox_location_id" id="dropdown_payment_squareup_sandbox_location_id" class="form-control" {% if not payment_squareup_sandbox_locations %} disabled {% endif %}>*/
/*                                             {% for location in payment_squareup_sandbox_locations %}*/
/*                                                 <option value="{{ location.id }}" {% if location.id == payment_squareup_sandbox_location_id %} selected {% endif %}>{{ location.name }}</option>*/
/*                                             {% endfor %}*/
/*                                         </select>*/
/*                                         {% if error_location %}*/
/*                                             <div class="text-danger">{{ error_location }}</div>*/
/*                                         {% endif %}*/
/*                                     </div>*/
/*                                 </div>*/
/*                             </fieldset>*/
/*                             <fieldset>*/
/*                                 <legend>{{ text_transaction_statuses }}</legend>*/
/*                                 <div class="form-group">*/
/*                                     <label class="col-sm-2 control-label" for="dropdown_payment_squareup_status_authorized"><span data-toggle="tooltip" title="{{ payment_squareup_status_comment_authorized }}">{{ entry_status_authorized }}</span></label>*/
/*                                     <div class="col-sm-10">*/
/*                                         <select name="payment_squareup_status_authorized" id="dropdown_payment_squareup_status_authorized" class="form-control">*/
/*                                             {% for order_status in order_statuses %}*/
/*                                                 <option value="{{ order_status.order_status_id }}" {% if order_status.order_status_id == payment_squareup_status_authorized %} selected {% endif %}>{{ order_status.name }}</option>*/
/*                                             {% endfor %}*/
/*                                         </select>*/
/*                                     </div>*/
/*                                 </div>*/
/*                                 <div class="form-group">*/
/*                                     <label class="col-sm-2 control-label" for="dropdown_payment_squareup_status_captured"><span data-toggle="tooltip" title="{{ payment_squareup_status_comment_captured }}">{{ entry_status_captured }}</span></label>*/
/*                                     <div class="col-sm-10">*/
/*                                         <select name="payment_squareup_status_captured" id="dropdown_payment_squareup_status_captured" class="form-control">*/
/*                                             {% for order_status in order_statuses %}*/
/*                                                 <option value="{{ order_status.order_status_id }}" {% if order_status.order_status_id == payment_squareup_status_captured %} selected {% endif %}>{{ order_status.name }}</option>*/
/*                                             {% endfor %}*/
/*                                         </select>*/
/*                                     </div>*/
/*                                 </div>*/
/*                                 <div class="form-group">*/
/*                                     <label class="col-sm-2 control-label" for="dropdown_payment_squareup_status_voided"><span data-toggle="tooltip" title="{{ payment_squareup_status_comment_voided }}">{{ entry_status_voided }}</span></label>*/
/*                                     <div class="col-sm-10">*/
/*                                         <select name="payment_squareup_status_voided" id="dropdown_payment_squareup_status_voided" class="form-control">*/
/*                                             {% for order_status in order_statuses %}*/
/*                                                 <option value="{{ order_status.order_status_id }}" {% if order_status.order_status_id == payment_squareup_status_voided %} selected {% endif %}>{{ order_status.name }}</option>*/
/*                                             {% endfor %}*/
/*                                         </select>*/
/*                                     </div>*/
/*                                 </div>*/
/*                                 <div class="form-group">*/
/*                                     <label class="col-sm-2 control-label" for="dropdown_payment_squareup_status_failed"><span data-toggle="tooltip" title="{{ payment_squareup_status_comment_failed }}">{{ entry_status_failed }}</span></label>*/
/*                                     <div class="col-sm-10">*/
/*                                         <select name="payment_squareup_status_failed" id="dropdown_payment_squareup_status_failed" class="form-control">*/
/*                                             {% for order_status in order_statuses %}*/
/*                                                 <option value="{{ order_status.order_status_id }}" {% if order_status.order_status_id == payment_squareup_status_failed %} selected {% endif %}>{{ order_status.name }}</option>*/
/*                                             {% endfor %}*/
/*                                         </select>*/
/*                                     </div>*/
/*                                 </div>*/
/*                             </fieldset>*/
/*                         </div>*/
/*                         <div class="tab-pane" id="tab-transaction">*/
/*                             <div id="transaction-alert" data-message="{{ text_loading }}"></div>*/
/*                             <div class="text-right margin-bottom">*/
/*                             </div>*/
/*                             <div class="table-responsive">*/
/*                                 <table class="table table-bordered table-hover">*/
/*                                     <thead>*/
/*                                         <tr>*/
/*                                             <th class="text-left hidden-xs">{{ column_transaction_id }}</th> */
/*                                             <th class="text-left">{{ column_customer }}</th>*/
/*                                             <th class="text-left hidden-xs">{{ column_order_id }}</th>*/
/*                                             <th class="text-left hidden-xs">{{ column_type }}</th>*/
/*                                             <th class="text-left hidden-xs">{{ column_amount }}</th>*/
/*                                             <th class="text-left hidden-xs">{{ column_refunds }}</th>*/
/*                                             <th class="text-left hidden-xs hidden-sm">{{ column_ip }}</th>*/
/*                                             <th class="text-left">{{ column_date_created }}</th>*/
/*                                             <th class="text-right">{{ column_action }}</th>*/
/*                                         </tr>*/
/*                                     </thead>*/
/*                                     <tbody id="transactions">*/
/*                                     </tbody>*/
/*                                 </table>*/
/*                                 <div id="transactions_pagination"></div>*/
/*                             </div>*/
/*                         </div>*/
/*                         <div class="tab-pane" id="tab-cron">*/
/*                             <fieldset>*/
/*                                 <legend>{{ text_executables }}</legend>*/
/*                                 <div class="alert alert-info"><i class="fa fa-info-circle"></i> {{ text_recurring_info }}</div>*/
/*                                 <div class="form-group">*/
/*                                     <label class="col-sm-2 control-label"><span data-toggle="tooltip" data-original-title="{{ help_local_cron }}">{{ text_local_cron }}</span></label>*/
/*                                     <div class="col-sm-10">*/
/*                                         <input disabled type="text" class="form-control" value="{{ payment_squareup_cron_command }}" />*/
/*                                     </div>*/
/*                                 </div>*/
/*                                 <div class="form-group">*/
/*                                     <label class="col-sm-2 control-label"><span data-toggle="tooltip" data-original-title="{{ help_remote_cron }}">{{ text_remote_cron }}</span></label>*/
/*                                     <div class="col-sm-10">*/
/*                                         <div class="input-group">*/
/*                                             <input disabled type="text" name="payment_squareup_cron_url" id="input_payment_squareup_cron_url" class="form-control" value="" />*/
/*                                             <div data-toggle="tooltip" data-original-title="{{ text_refresh_token }}" class="input-group-addon btn btn-primary" id="refresh-cron-token">*/
/*                                                 <i class="fa fa-refresh"></i>*/
/*                                             </div>*/
/*                                         </div>*/
/*                                         <input id="input_payment_squareup_cron_token" type="hidden" name="payment_squareup_cron_token" value="{{ payment_squareup_cron_token }}" />*/
/*                                     </div>*/
/*                                 </div>*/
/*                                 <div class="form-group required">*/
/*                                     <label class="col-sm-2 control-label" for="checkbox_payment_squareup_cron_acknowledge">{{ entry_setup_confirmation }}</label>*/
/*                                     <div class="col-sm-10">*/
/*                                         <label class="checkbox-inline">*/
/*                                             <input id="checkbox_payment_squareup_cron_acknowledge" type="checkbox" value="1" {% if payment_squareup_cron_acknowledge %} checked {% endif %} name="payment_squareup_cron_acknowledge" /> {{ text_acknowledge_cron }}*/
/*                                         </label>*/
/* */
/*                                         {% if error_cron_acknowledge %}*/
/*                                             <div class="text-danger">{{ error_cron_acknowledge }}</div>*/
/*                                         {% endif %}*/
/*                                     </div>*/
/*                                 </div>*/
/*                             </fieldset>*/
/*                             <fieldset>*/
/*                                 <legend>{{ text_admin_notifications }}</legend>*/
/*                                 <div class="form-group">*/
/*                                     <label class="control-label col-sm-2" for="dropdown_payment_squareup_cron_email_status"><span data-toggle="tooltip" data-original-title="{{ help_cron_email_status }}">{{ text_cron_email_status }}</span></label>*/
/*                                     <div class="col-sm-10">*/
/*                                         <select id="dropdown_payment_squareup_cron_email_status" name="payment_squareup_cron_email_status" class="form-control">*/
/*                                             <option value="1" {% if payment_squareup_cron_email_status == '1' %} selected {% endif %}>{{ text_enabled }}</option>*/
/*                                             <option value="0" {% if payment_squareup_cron_email_status == '0' %} selected {% endif %}>{{ text_disabled }}</option>*/
/*                                         </select>*/
/*                                     </div>*/
/*                                 </div>*/
/*                                 <div class="form-group required">*/
/*                                     <label class="col-sm-2 control-label" for="input_payment_squareup_cron_email"><span data-toggle="tooltip" data-original-title="{{ help_cron_email }}">{{ text_cron_email }}</span></label>*/
/*                                     <div class="col-sm-10">*/
/*                                         <input id="input_payment_squareup_cron_email" name="payment_squareup_cron_email" type="text" class="form-control" value="{{ payment_squareup_cron_email }}"/>*/
/*                                         {% if error_cron_email %}*/
/*                                             <div class="text-danger">{{ error_cron_email }}</div>*/
/*                                         {% endif %}*/
/*                                     </div>*/
/*                                 </div>*/
/*                             </fieldset>*/
/*                         </div>*/
/*                         <div class="tab-pane" id="tab-recurring">*/
/*                             <div class="form-group">*/
/*                                 <label class="control-label col-sm-2" for="dropdown_payment_squareup_recurring_status"><span data-toggle="tooltip" data-original-title="{{ help_recurring_status }}">{{ text_recurring_status }}</span></label>*/
/*                                 <div class="col-sm-10">*/
/*                                     <select id="dropdown_payment_squareup_recurring_status" name="payment_squareup_recurring_status" class="form-control">*/
/*                                         <option value="1" {% if payment_squareup_recurring_status == '1' %} selected {% endif %}>{{ text_enabled }}</option>*/
/*                                         <option value="0" {% if payment_squareup_recurring_status == '0' %} selected {% endif %}>{{ text_disabled }}</option>*/
/*                                     </select>*/
/*                                 </div>*/
/*                             </div>*/
/*                             <fieldset>*/
/*                                 <legend>{{ text_customer_notifications }}</legend>*/
/*                                 <div class="form-group">*/
/*                                     <label class="control-label col-sm-2" for="dropdown_payment_squareup_notify_recurring_success"><span data-toggle="tooltip" data-original-title="{{ help_notify_recurring_success }}">{{ text_notify_recurring_success }}</span></label>*/
/*                                     <div class="col-sm-10">*/
/*                                         <select id="dropdown_payment_squareup_notify_recurring_success" name="payment_squareup_notify_recurring_success" class="form-control">*/
/*                                             <option value="1" {% if payment_squareup_notify_recurring_success == '1' %} selected {% endif %}>{{ text_enabled }}</option>*/
/*                                             <option value="0" {% if payment_squareup_notify_recurring_success == '0' %} selected {% endif %}>{{ text_disabled }}</option>*/
/*                                         </select>*/
/*                                     </div>*/
/*                                 </div>*/
/*                                 <div class="form-group">*/
/*                                     <label class="control-label col-sm-2" for="dropdown_payment_squareup_notify_recurring_fail"><span data-toggle="tooltip" data-original-title="{{ help_notify_recurring_fail }}">{{ text_notify_recurring_fail }}</span></label>*/
/*                                     <div class="col-sm-10">*/
/*                                         <select id="dropdown_payment_squareup_notify_recurring_fail" name="payment_squareup_notify_recurring_fail" class="form-control">*/
/*                                             <option value="1" {% if payment_squareup_notify_recurring_fail == '1' %} selected {% endif %}>{{ text_enabled }}</option>*/
/*                                             <option value="0" {% if payment_squareup_notify_recurring_fail == '0' %} selected {% endif %}>{{ text_disabled }}</option>*/
/*                                         </select>*/
/*                                     </div>*/
/*                                 </div>*/
/*                             </fieldset>*/
/*                         </div>*/
/*                     </div>*/
/*                 </form>*/
/*             </div>*/
/*         </div>*/
/*     </div>*/
/*     <div class="modal fade" id="squareup-confirm-modal">*/
/*         <div class="modal-dialog">*/
/*             <div class="modal-content">*/
/*                 <div class="modal-header">*/
/*                     <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>*/
/*                     <h4 class="modal-title">{{ text_confirm_action }}</h4>*/
/*                 </div>*/
/*                 <div class="modal-body">*/
/*                     <h4 id="squareup-confirm-modal-content"></h4>*/
/*                 </div>*/
/*                 <div class="modal-footer">*/
/*                     <button type="button" class="btn btn-default" data-dismiss="modal">{{ text_close }}</button>*/
/*                     <button id="squareup-confirm-ok" type="button" class="btn btn-primary">{{ text_ok }}</button>*/
/*                 </div>*/
/*             </div>*/
/*         </div>*/
/*     </div>*/
/*     <div class="modal fade" id="squareup-refund-modal">*/
/*         <div class="modal-dialog">*/
/*             <div class="modal-content">*/
/*                 <div class="modal-header">*/
/*                     <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>*/
/*                     <h4 class="modal-title">{{ text_refund_details }}</h4>*/
/*                 </div>*/
/*                 <div class="modal-body">*/
/*                     <div class="form-group">*/
/*                         <label class="control-label" id="squareup-refund-modal-content-reason"></label>*/
/*                         <textarea class="form-control" id="squareup-refund-reason" required></textarea>*/
/*                     </div>*/
/*                     <div class="form-group">*/
/*                         <label class="control-label" id="squareup-refund-modal-content-amount"></label>*/
/*                         <input class="form-control" type="text" id="squareup-refund-amount" required />*/
/*                     </div>*/
/*                 </div>*/
/*                 <div class="modal-footer">*/
/*                     <button type="button" class="btn btn-default" data-dismiss="modal">{{ text_close }}</button>*/
/*                     <button id="squareup-refund-ok" type="button" class="btn btn-primary">{{ text_ok }}</button>*/
/*                 </div>*/
/*             </div>*/
/*         </div>*/
/*     </div>*/
/* </div>*/
/* <script type="text/javascript">*/
/* $(document).ready(function() {*/
/*     var triggerConnectButtons = function() {*/
/*         if ($('#input_payment_squareup_client_id').val() != '' && $('#input_payment_squareup_client_secret').val() != '') {*/
/*             $('.btn-connect').removeClass('disabled');*/
/*         } else {*/
/*             $('.btn-connect').addClass('disabled');*/
/*         }*/
/*     }*/
/* */
/*     var setCronUrl = function() {*/
/*         $('#input_payment_squareup_cron_url').val(*/
/*             "{{ payment_squareup_cron_url }}".replace('{CRON_TOKEN}', $('#input_payment_squareup_cron_token').val())*/
/*         );*/
/*     }*/
/* */
/*     var randomString = function() {*/
/*         return (Math.random() * 100).toString(16).replace('.', '');*/
/*     }*/
/* */
/*     var onConnectClick = function(event) {*/
/*         if ($('#input_payment_squareup_client_id').val() != '{{ payment_squareup_client_id }}' || $('#input_payment_squareup_client_secret').val() != '{{ payment_squareup_client_secret }}') {*/
/*             event.preventDefault();*/
/*             event.stopPropagation();*/
/* */
/*             $('form').attr('action','{{ action_save_auth }}').submit();*/
/*         }*/
/*     }*/
/* */
/*     var listTransactions = function(page) {*/
/*         $.ajax({*/
/*           url : '{{ url_list_transactions }}'.replace('{PAGE}', page ? page : transactionListPage),*/
/*           dataType : 'json',*/
/*           beforeSend : function() {*/
/*             $('#refresh_transactions').button('loading');*/
/*             $('#transactions_pagination').empty();*/
/*             $('#transactions').html('<tr><td colspan="9" class="text-center"><i class="fa fa-circle-o-notch fa-spin"></i>&nbsp;{{ text_loading }}</td></tr>');*/
/*           },*/
/*           success : function(data) {*/
/*             var html = '';*/
/* */
/*             if (data.transactions.length) {*/
/*               for (var i in data.transactions) {*/
/*                 var row = data.transactions[i];*/
/* */
/*                 html += '<tr>';*/
/*                 html += '<td class="text-left hidden-xs">' + row.transaction_id + '</td>';*/
/*                 html += '<td class="text-left hidden-xs">' + row.customer + '</td>';*/
/*                 html += '<td class="text-left"><a target="_blank" href="' + row.url_order + '">' + row.order_id + '</td>';*/
/*                 html += '<td class="text-left hidden-xs">' + row.type + '</td>';*/
/*                 html += '<td class="text-left hidden-xs">' + row.amount + '</td>';*/
/*                 html += '<td class="text-left hidden-xs">' + row.num_refunds + '</td>';*/
/*                 html += '<td class="text-left hidden-xs hidden-sm">' + row.ip + '</td>';*/
/*                 html += '<td class="text-left">' + row.date_created + '</td>';*/
/*                 html += '<td class="text-right">';*/
/* */
/*                 switch (row.type) {*/
/*                     case "AUTHORIZED" : {*/
/*                         html += '<a class="btn btn-success" data-url-transaction-capture="' + row.url_capture + '" data-confirm-capture="' + row.confirm_capture + '">{{ text_capture }}</a> ';*/
/*                         html += '<a class="btn btn-warning" data-url-transaction-void="' + row.url_void + '" data-confirm-void="' + row.confirm_void + '">{{ text_void }}</a> ';*/
/*                     } break;*/
/* */
/*                     case "CAPTURED" : {*/
/*                         html += '<a class="btn btn-danger" data-url-transaction-refund="' + row.url_refund + '" data-confirm-refund="' + row.confirm_refund + '" data-insert-amount="' + row.insert_amount + '">{{ text_refund }}</a> ';*/
/*                     } break;*/
/*                 }*/
/* */
/*                 html += ' <a class="btn btn-info" href="' + row.url_info + '">{{ text_view }}</a>';*/
/*                 html += '</td>';*/
/*                 html += '</tr>';*/
/*               }*/
/*             } else {*/
/*               html += '<tr>';*/
/*               html += '<td class="text-center" colspan="9">{{ text_no_transactions }}</td>';*/
/*               html += '</tr>';*/
/*             }*/
/* */
/*             $('#transactions').html(html);*/
/*             */
/*             $('#transactions_pagination').html(data.pagination).find('a[href]').each(function(index,element) {*/
/*               $(this).click(function(e) {*/
/*                 e.preventDefault();*/
/* */
/*                 transactionListPage = isNaN($(this).attr('href')) ? 1 : $(this).attr('href');*/
/* */
/*                 listTransactions();*/
/*               })*/
/*             });*/
/*           },*/
/*           complete : function() {*/
/*             $('#refresh_transactions').button('reset');*/
/*           }*/
/*         });*/
/*     }*/
/* */
/*     var transactionLoading = function() {*/
/*         var message = $('#transaction-alert').attr('data-message');*/
/* */
/*         $('#transaction-alert').html('<div class="text-center alert alert-info"><i class="fa fa-circle-o-notch fa-spin"></i>&nbsp;' + message + '</div>');*/
/*     }*/
/* */
/*     var transactionError = function(message) {*/
/*         $('#transaction-alert').html('<div class="alert alert-danger"><button type="button" class="close" data-dismiss="alert" aria-label="X"><span aria-hidden="true">&times;</span></button><i class="fa fa-exclamation-circle"></i>&nbsp;' + message + '</div>');*/
/*     }*/
/* */
/*     var transactionSuccess = function(message) {*/
/*         $('#transaction-alert').html('<div class="alert alert-success"><button type="button" class="close" data-dismiss="alert" aria-label="X"><span aria-hidden="true">&times;</span></button><i class="fa fa-exclamation-circle"></i>&nbsp;' + message + '</div>');*/
/*     }*/
/* */
/*     var addOrderHistory = function(data, success_callback) {*/
/*         $.ajax({*/
/*             url: '{{ catalog }}index.php?route=api/order/history&api_token={{ api_token }}&order_id=' + data.order_id,*/
/*             type: 'post',*/
/*             dataType: 'json',*/
/*             data: data,*/
/*             success: function(json) {*/
/*                 if (json['error']) {*/
/*                     transactionError(json['error']);*/
/*                     enableTransactionButtons();*/
/*                 }*/
/* */
/*                 if (json['success']) {*/
/*                     success_callback();*/
/*                 }*/
/*             },*/
/*             error: function(xhr, ajaxOptions, thrownError) {*/
/*                 transactionError(thrownError + "\r\n" + xhr.statusText + "\r\n" + xhr.responseText);*/
/*                 enableTransactionButtons();*/
/*             }*/
/*         });*/
/*     }*/
/* */
/*     var transactionRequest = function(type, url, data) {*/
/*         $.ajax({*/
/*             url : url,*/
/*             dataType : 'json',*/
/*             type : type,*/
/*             data : data,*/
/*             beforeSend : transactionLoading,*/
/*             success : function(json) {*/
/*                 if (json.error) {*/
/*                     transactionError(json.error);*/
/*                     enableTransactionButtons();*/
/*                 }*/
/* */
/*                 if (json.success && json.order_history_data) {*/
/*                     addOrderHistory(json.order_history_data, function() {*/
/*                         transactionSuccess(json.success);*/
/*                         listTransactions();*/
/*                     });*/
/*                 }*/
/*             },*/
/*             error : function(xhr, ajaxSettings, thrownError) {*/
/*                 transactionError(thrownError + "\r\n" + xhr.statusText + "\r\n" + xhr.responseText);*/
/*                 enableTransactionButtons();*/
/*             }*/
/*         });*/
/*     }*/
/* */
/*     var disableTransactionButtons = function() {*/
/*         $('*[data-url-transaction-capture], *[data-url-transaction-void], *[data-url-transaction-refund]').attr('disabled', true);*/
/*     }*/
/* */
/*     var enableTransactionButtons = function() {*/
/*         $('*[data-url-transaction-capture], *[data-url-transaction-void], *[data-url-transaction-refund]').attr('disabled', false);*/
/*     }*/
/* */
/*     var modalConfirm = function(url, text) {*/
/*         var modal = '#squareup-confirm-modal';*/
/*         var content = '#squareup-confirm-modal-content';*/
/*         var button = '#squareup-confirm-ok';*/
/* */
/*         $(content).html(text);*/
/*         $(button).unbind().click(function() {*/
/*             disableTransactionButtons();*/
/* */
/*             $(modal).modal('hide');*/
/* */
/*             transactionRequest('GET', url);*/
/*         });*/
/*         */
/*         $(modal).modal('show');*/
/*     }*/
/* */
/*     var refundInputValidate = function(reason_input, amount_input) {*/
/*         var result = true;*/
/* */
/*         if (!$(reason_input)[0].checkValidity()) {*/
/*             $(reason_input).closest('.form-group').addClass('has-error');*/
/*             result = false;*/
/*         } else {*/
/*             $(reason_input).closest('.form-group').removeClass('has-error');*/
/*         }*/
/* */
/*         if (!$(amount_input)[0].checkValidity()) {*/
/*             $(amount_input).closest('.form-group').addClass('has-error');*/
/*             result = false;*/
/*         } else {*/
/*             $(amount_input).closest('.form-group').removeClass('has-error');*/
/*         }*/
/* */
/*         return result;*/
/*     }*/
/* */
/*     var modalRefund = function(url, text_reason, text_amount) {*/
/*         var modal = '#squareup-refund-modal';*/
/*         var content_reason = '#squareup-refund-modal-content-reason';*/
/*         var content_amount = '#squareup-refund-modal-content-amount';*/
/*         var button = '#squareup-refund-ok';*/
/*         var reason_input = '#squareup-refund-reason';*/
/*         var amount_input = '#squareup-refund-amount';*/
/* */
/*         $(content_reason).html(text_reason);*/
/*         $(content_amount).html(text_amount);*/
/* */
/*         $(reason_input).val('');*/
/*         $(amount_input).val('');*/
/* */
/*         $(button).unbind().click(function() {*/
/*             if (!refundInputValidate(reason_input, amount_input)) {*/
/*                 return;*/
/*             }*/
/* */
/*             disableTransactionButtons();*/
/* */
/*             $(modal).modal('hide');*/
/* */
/*             transactionRequest('POST', url, {*/
/*                 reason : $(reason_input).val(),*/
/*                 amount : $(amount_input).val()*/
/*             });*/
/*         });*/
/*         */
/*         $(modal).modal('show');*/
/*     }*/
/* */
/*     var transactionListPage = 1;*/
/* */
/*     $('.nav-tabs a[href="#{{ tab }}"]').tab('show');*/
/* */
/*     $('#dropdown_payment_squareup_enable_sandbox')*/
/*         .change(function() {*/
/*             if ($(this).val() == '0') {*/
/*                 $('#sandbox_settings').slideUp();*/
/*                 $('#dropdown_payment_squareup_location_id').show();*/
/*                 $('#dropdown_payment_squareup_sandbox_location_id').hide();*/
/*             } else {*/
/*                 $('#sandbox_settings').slideDown();*/
/*                 $('#dropdown_payment_squareup_location_id').hide();*/
/*                 $('#dropdown_payment_squareup_sandbox_location_id').show();*/
/*             }*/
/*         })*/
/*         .trigger('change');*/
/* */
/*     $('#input_payment_squareup_client_id, #input_payment_squareup_client_secret')*/
/*         .change(triggerConnectButtons)*/
/*         .keyup(triggerConnectButtons)*/
/*         .trigger('change');*/
/* */
/*     $('#refresh-cron-token').click(function() {*/
/*         $('#input_payment_squareup_cron_token').val(randomString() + randomString());*/
/*         setCronUrl();*/
/*     });*/
/* */
/*     $('#connect-button').click(onConnectClick);*/
/*     */
/*     $('#reconnect-button').click(onConnectClick);*/
/* */
/*     $(document).on('click', '*[data-url-transaction-capture]', function() {*/
/*         if ($(this).attr('disabled')) return;*/
/* */
/*         modalConfirm(*/
/*             $(this).attr('data-url-transaction-capture'),*/
/*             $(this).attr('data-confirm-capture')*/
/*         );*/
/*     });*/
/*         */
/*     $(document).on('click', '*[data-url-transaction-void]', function() {*/
/*         if ($(this).attr('disabled')) return;*/
/* */
/*         modalConfirm(*/
/*             $(this).attr('data-url-transaction-void'),*/
/*             $(this).attr('data-confirm-void')*/
/*         );*/
/*     });*/
/* */
/*     $(document).on('click', '*[data-url-transaction-refund]', function() {*/
/*         if ($(this).attr('disabled')) return;*/
/* */
/*         modalRefund(*/
/*             $(this).attr('data-url-transaction-refund'),*/
/*             $(this).attr('data-confirm-refund'),*/
/*             $(this).attr('data-insert-amount')*/
/*         );*/
/*     });*/
/*     */
/*     setCronUrl();*/
/* */
/*     listTransactions();*/
/* });*/
/* </script>*/
/* {{ footer }}*/
