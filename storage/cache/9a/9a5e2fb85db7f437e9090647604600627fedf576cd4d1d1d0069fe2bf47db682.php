<?php

/* default/template/extension/payment/skrill.twig */
class __TwigTemplate_48a141ad408330af6133d357be5b2b2b158d396956f6a6c31f2a43aba75b28c5 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 1
        echo "<form action=\"";
        echo (isset($context["action"]) ? $context["action"] : null);
        echo "\" method=\"post\">
  <input type=\"hidden\" name=\"pay_to_email\" value=\"";
        // line 2
        echo (isset($context["pay_to_email"]) ? $context["pay_to_email"] : null);
        echo "\" />
  <input type=\"hidden\" name=\"recipient_description\" value=\"";
        // line 3
        echo (isset($context["description"]) ? $context["description"] : null);
        echo "\" />
  <input type=\"hidden\" name=\"transaction_id\" value=\"";
        // line 4
        echo (isset($context["transaction_id"]) ? $context["transaction_id"] : null);
        echo "\" />
  <input type=\"hidden\" name=\"return_url\" value=\"";
        // line 5
        echo (isset($context["return_url"]) ? $context["return_url"] : null);
        echo "\" />
  <input type=\"hidden\" name=\"cancel_url\" value=\"";
        // line 6
        echo (isset($context["cancel_url"]) ? $context["cancel_url"] : null);
        echo "\" />
  <input type=\"hidden\" name=\"status_url\" value=\"";
        // line 7
        echo (isset($context["status_url"]) ? $context["status_url"] : null);
        echo "\" />
  <input type=\"hidden\" name=\"language\" value=\"";
        // line 8
        echo (isset($context["language"]) ? $context["language"] : null);
        echo "\" />
  <input type=\"hidden\" name=\"logo_url\" value=\"";
        // line 9
        echo (isset($context["logo"]) ? $context["logo"] : null);
        echo "\" />
  <input type=\"hidden\" name=\"pay_from_email\" value=\"";
        // line 10
        echo (isset($context["pay_from_email"]) ? $context["pay_from_email"] : null);
        echo "\" />
  <input type=\"hidden\" name=\"firstname\" value=\"";
        // line 11
        echo (isset($context["firstname"]) ? $context["firstname"] : null);
        echo "\" />
  <input type=\"hidden\" name=\"lastname\" value=\"";
        // line 12
        echo (isset($context["lastname"]) ? $context["lastname"] : null);
        echo "\" />
  <input type=\"hidden\" name=\"address\" value=\"";
        // line 13
        echo (isset($context["address"]) ? $context["address"] : null);
        echo "\" />
  <input type=\"hidden\" name=\"address2\" value=\"";
        // line 14
        echo (isset($context["address2"]) ? $context["address2"] : null);
        echo "\" />
  <input type=\"hidden\" name=\"phone_number\" value=\"";
        // line 15
        echo (isset($context["phone_number"]) ? $context["phone_number"] : null);
        echo "\" />
  <input type=\"hidden\" name=\"postal_code\" value=\"";
        // line 16
        echo (isset($context["postal_code"]) ? $context["postal_code"] : null);
        echo "\" />
  <input type=\"hidden\" name=\"city\" value=\"";
        // line 17
        echo (isset($context["city"]) ? $context["city"] : null);
        echo "\" />
  <input type=\"hidden\" name=\"state\" value=\"";
        // line 18
        echo (isset($context["state"]) ? $context["state"] : null);
        echo "\" />
  <input type=\"hidden\" name=\"country\" value=\"";
        // line 19
        echo (isset($context["country"]) ? $context["country"] : null);
        echo "\" />
  <input type=\"hidden\" name=\"amount\" value=\"";
        // line 20
        echo (isset($context["amount"]) ? $context["amount"] : null);
        echo "\" />
  <input type=\"hidden\" name=\"currency\" value=\"";
        // line 21
        echo (isset($context["currency"]) ? $context["currency"] : null);
        echo "\" />
  <input type=\"hidden\" name=\"detail1_text\" value=\"";
        // line 22
        echo (isset($context["detail1_text"]) ? $context["detail1_text"] : null);
        echo "\" />
  <input type=\"hidden\" name=\"merchant_fields\" value=\"order_id\" />
  <input type=\"hidden\" name=\"order_id\" value=\"";
        // line 24
        echo (isset($context["order_id"]) ? $context["order_id"] : null);
        echo "\" />
  <input type=\"hidden\" name=\"platform\" value=\"";
        // line 25
        echo (isset($context["platform"]) ? $context["platform"] : null);
        echo "\" />
  <div class=\"buttons\">
    <div class=\"pull-right\">
      <input type=\"submit\" value=\"";
        // line 28
        echo (isset($context["button_confirm"]) ? $context["button_confirm"] : null);
        echo "\" class=\"btn btn-primary\" />
    </div>
  </div>
</form>
";
    }

    public function getTemplateName()
    {
        return "default/template/extension/payment/skrill.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  119 => 28,  113 => 25,  109 => 24,  104 => 22,  100 => 21,  96 => 20,  92 => 19,  88 => 18,  84 => 17,  80 => 16,  76 => 15,  72 => 14,  68 => 13,  64 => 12,  60 => 11,  56 => 10,  52 => 9,  48 => 8,  44 => 7,  40 => 6,  36 => 5,  32 => 4,  28 => 3,  24 => 2,  19 => 1,);
    }
}
/* <form action="{{ action }}" method="post">*/
/*   <input type="hidden" name="pay_to_email" value="{{ pay_to_email }}" />*/
/*   <input type="hidden" name="recipient_description" value="{{ description }}" />*/
/*   <input type="hidden" name="transaction_id" value="{{ transaction_id }}" />*/
/*   <input type="hidden" name="return_url" value="{{ return_url }}" />*/
/*   <input type="hidden" name="cancel_url" value="{{ cancel_url }}" />*/
/*   <input type="hidden" name="status_url" value="{{ status_url }}" />*/
/*   <input type="hidden" name="language" value="{{ language }}" />*/
/*   <input type="hidden" name="logo_url" value="{{ logo }}" />*/
/*   <input type="hidden" name="pay_from_email" value="{{ pay_from_email }}" />*/
/*   <input type="hidden" name="firstname" value="{{ firstname }}" />*/
/*   <input type="hidden" name="lastname" value="{{ lastname }}" />*/
/*   <input type="hidden" name="address" value="{{ address }}" />*/
/*   <input type="hidden" name="address2" value="{{ address2 }}" />*/
/*   <input type="hidden" name="phone_number" value="{{ phone_number }}" />*/
/*   <input type="hidden" name="postal_code" value="{{ postal_code }}" />*/
/*   <input type="hidden" name="city" value="{{ city }}" />*/
/*   <input type="hidden" name="state" value="{{ state }}" />*/
/*   <input type="hidden" name="country" value="{{ country }}" />*/
/*   <input type="hidden" name="amount" value="{{ amount }}" />*/
/*   <input type="hidden" name="currency" value="{{ currency }}" />*/
/*   <input type="hidden" name="detail1_text" value="{{ detail1_text }}" />*/
/*   <input type="hidden" name="merchant_fields" value="order_id" />*/
/*   <input type="hidden" name="order_id" value="{{ order_id }}" />*/
/*   <input type="hidden" name="platform" value="{{ platform }}" />*/
/*   <div class="buttons">*/
/*     <div class="pull-right">*/
/*       <input type="submit" value="{{ button_confirm }}" class="btn btn-primary" />*/
/*     </div>*/
/*   </div>*/
/* </form>*/
/* */
